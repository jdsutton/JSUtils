import('Matrix.js');
import('Vector.js');

/**
 * @class
 */
class CoordinateSystem {

    /**
     * @constructor
     */
    constructor(x, y, z, xRot, yRot, zRot) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.xRot = xRot || 0;
        this.yRot = yRot || 0;
        this.zRot = zRot || 0;
    }

    /**
     * @public
     */
    copy() {
        return new CoordinateSystem(
            this.x,
            this.y,
            this.z,
            this.xRot,
            this.yRot,
            this.zRot,
        );
    }

    /**
     * @public
     */
    rotate(x, y, z) {
        this.xRot += x;
        this.yRot += y;
        this.zRot += z;
    }

    /**
     * @public
     */
    getPosition() {
        return new Vector(this.x, this.y, this.z);
    }

    /**
     * @public
     */
    getRotation() {
        return new Vector(this.xRot, this.yRot, this.zRot);
    }

    /**
     * @public
     */
    static computeTranslationTransformation(A, B) {
        return Matrix.translationalTransform(
            new Vector(B.x-A.x, B.y-A.y, B.z-A.z));
    }

    /**
     * @public
     */
    static computeRotationTransformation(A, B) {
        return Matrix.rotationalTransform(
            new Vector(B.xRot-A.xRot, B.yRot-A.yRot, B.zRot-A.zRot));
    }

    /**
     * @public
     * Computes a transformation matrix from A to B.
     * @param {CoordinateSystem} A
     * @param {CoordinateSystem} B
     */
    static computeTransformation(A, B) {
        const translationFromBToA = Matrix.translationalTransform(
            new Vector(B.x-A.x, B.y-A.y, B.z-A.z));
        const rotationFromBToA = Matrix.rotationalTransform(
            new Vector(B.xRot-A.xRot, B.yRot-A.yRot, B.zRot-A.zRot));

        return translationFromBToA.multiply(rotationFromBToA);
    };
}