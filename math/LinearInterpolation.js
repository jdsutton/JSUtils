/**
 * @class
 */
class LinearInterpolation {

    /**
     * @public
     */
    static compute(a0, a1, w) {
        return (1.0 - w) * a0 + w * a1;
    }
}