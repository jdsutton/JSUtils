import('CoordinateSystem.js');
import('Matrix.js');
import('Vector.js');

/**
 * @class
 */
class KinematicJoints {

    /**
     * @constructor
     */
    constructor(x, y, z) {
        this._base = new Vector(x, y, z);
        this._coords = [];
        this._axes = [];
    }

    /**
     * @public
     */
    get numJoints() {
        return this._coords.length;
    }

    /**
     * @public
     */
    copy() {
        let result = new KinematicJoints(this._base.x, this._base.y, this._base.z);

        for (let i in this._axes) {
            result._coords.push(this._coords[i].copy());
            result._axes.push(this._axes[i].copy());
        }

        return result;
    }

    /**
     * @public
     */
    getPointOfRotation(index) {
        let result = new Matrix([[0, 0, 0, 1]]).transpose();
        let transformations = []
        const coords = this._coords.concat([new CoordinateSystem(0, 0, 0)]);
        const base = new CoordinateSystem(0, 0, 0);

        for (let i = 0; i < index + 1; i++) {
            const t = CoordinateSystem.computeTransformation(
                base, coords[i]);

            transformations.push(t);
        }

        for (let i = transformations.length - 1; i >= 0; i--) {
            result = transformations[i].multiply(result);
        }

        const col = result.getColumn(0);

        return new Vector(
            this._base.x + col[0],
            this._base.y + col[1],
            this._base.z + col[2],
        );
    };

    /**
     * @private
     */
    _gradient(fromSys, targetVector) {
        const index = this._coords.indexOf(fromSys);
        const joint = this.getPointOfRotation(index);
        const axisOfRotation = this._axes[index];
        const tip = this.getTipPosition();
        
        // Vector from base joint to tip of arm.
        const toTip = tip.subtract(joint);
        
        // Vector from tip to target.
        const toTarget = targetVector.subtract(tip);
        const movementVector = toTip.crossProduct(axisOfRotation);

        return movementVector.dotProduct(toTarget);
    };

    /**
     * @public
     */
    moveTipToPosition(x, y, z, maxDistance, rate, maxAttempts) {
        const targetVector = new Vector(x, y, z);
        maxDistance = maxDistance || 0.1;
        rate = (rate || 1) * 0.000001;
        maxAttempts = maxAttempts || 5000;

        // this._gradientDescentSolve(targetVector, maxDistance, rate, maxAttempts);
        this._solveFast(targetVector, maxDistance, rate, maxAttempts);

        return this;
    }

    /**
     * @private
     */
    _solveFast(targetVector, maxDistance, rate, maxAttempts) {
        const tip = this.getTipPosition();
        let dist = this._getDist(targetVector);
        let i = 0;
        let speed = [];
        let oldGradients = [];

        for (let i in this._coords) {
            speed.push(rate);
            oldGradients.push(0);
        }

        while (i < maxAttempts && dist > maxDistance) {
            for (let i in this._coords) {
                const grad = this._incrementalGradient(i, targetVector);
                const oldGradient = oldGradients[i];
                const dGrad = grad - oldGradient;
                const s = speed[i];

                // Adjust speed.
                if (Math.sign(grad) !== Math.sign(oldGradient)) {
                    this.rotateJoint(i, s * oldGradient / dGrad);
                    speed[i] = 0;
                } else {
                    speed[i] += grad * rate;
                }

                oldGradients[i] = grad;
            }

            // Move.
            for (let i in speed) {
                this.rotateJoint(i, -speed[i]);
            }

            dist = this._getDist(targetVector);
            i++;
        }
    }

    /**
     * @private
     */
    _incrementalGradient(index, targetVector) {
        const plus = this.copy().rotateJoint(index, 1);
        const minus = this.copy().rotateJoint(index, -1);

        return plus._getDist(targetVector) - minus._getDist(targetVector);
    }

    /**
     * @private
     */
    _getDist(targetVector, v) {
        v = v || this.getTipPosition();
        const diff = v.subtract(targetVector);

        return Math.sqrt(
            Math.pow(diff.x, 2)
            + Math.pow(diff.y, 2)
            + Math.pow(diff.z, 2)
        );
    }

    /**
     * @private
     */
    _gradientDescentSolve(targetVector, maxDistance, rate, maxAttempts) {
        let i = 0;

        while (i < maxAttempts && this._getDist(targetVector) > maxDistance) {
            const gradients = this._coords.map(coord => 
                this._gradient(coord, targetVector));

            for (let i in this._coords) {
                const grad = gradients[i];
                const rot = this._axes[i]
                    .multiply(-1 * rate)
                    .multiply(grad);

                this._coords[i].rotate(rot.x, rot.y, rot.z);
            }

            i++;
        }
    }

    /**
     * @public
     */
    getTipPosition(index) {
        if (index === undefined) {
            index = this._coords.length - 1;
        }

        return this.getPointOfRotation(index + 1);
    }

    /**
     * @public
     */
    addJoint(w, h, L, axis) {
        const sys = new CoordinateSystem(w, h, L);

        this._coords.push(sys);
        this._axes.push(axis || new Vector(0, 0, 1));

        return this;
    }

    /**
     * @public
     */
    removeJoint(i) {
        this._coords.splice(i, 1);
        this._axes.splice(i, 1);

        return this;
    }

    /**
     * @public
     */
    rotateJoint(index, dTheta) {
        const rot = this._axes[index].multiply(dTheta);

        this._coords[index].rotate(rot.x, rot.y, rot.z);

        return this;
    }

    /**
     * @public
     */
    getJointRotation(index) {
        const joint = this._coords[index];

        return new Vector(joint.xRot, joint.yRot, joint.zRot);
    }

    /**
     * @public
     */
    getJoint(i) {
        return this._coords[i];
    }
}