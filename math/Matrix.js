/**
 * @class
 */
class Matrix {

    /**
     * @constructor
     */
    constructor(data) {
        const args = Array.from(arguments);

        if (args.length > 1) {
            this._data = [];

            args.forEach(arg => {this._data.push(arg)});
        } else {
            this._data = data || [];
        }
    }

    /**
     * @public
     */
    transpose() {
        let columns = [];

        for (let i = 0; i < this.columns; i++) {
            columns.push(this.getColumn(i));
        }

        return new Matrix(columns);
    }

    /**
     * @public
     */
    get columns() {
        if (!this._data[0]) return 0;

        return this._data[0].length;
    }

    /**
     * @public
     */
    get rows() {
        return this._data.length;
    }

    /**
     * @public
     */
    static zeros(rows, columns) {
        let data = [];

        for (let i = 0; i < rows; i++) {
            let row = [];

            for (let j = 0; j < columns; j++) {
                row.push(0);
            }

            data.push(row);
        }

        return new Matrix(data);
    }

    /**
     * @public
     * Sets all values to 0 in-place.
     */
    zero() {
        for (let i = 0; i < this.rows; i++) {
            for (let j = 0; j < this.columns; j++) {
                this._data[i][j] = 0;
            }
        }

        return this;
    }

    /**
     * @public
     * Scales all values to the given range in-place.
     */
    scale(minV, maxV) {
        let min = Infinity;
        let max = -Infinity;

        for (let y = 0; y < this.rows; y++) {
            for (let x = 0; x < this.columns; x++) {
                const value = this._data[y][x];
                min = Math.min(min, value);
                max = Math.max(max, value);
            }
        }

        const range = max - min;

        for (let y = 0; y < this.rows; y++) {
            for (let x = 0; x < this.columns; x++) {
                let value = this._data[y][x];

                // Scale to [0, 1].
                value = (value - min) / range;

                // Scale to target range.
                value = (value * (maxV-minV)) + minV;

                value = Math.max(value, minV);
                value = Math.min(value, maxV);

                this._data[y][x] = value;
            }
        }

        return this;
    }

    /**
     * @public
     */
    getColumn(i) {
        let result = [];

        for (let j = 0; j < this._data.length; j++) {
            result.push(this._data[j][i]);
        }

        return result;
    }

    /**
     * @public
     */
    get(col, row) {
        return this._data[row][col];
    }

    /**
     * @public
     */
    set(col, row, value) {
        this._data[row][col] = value;

        return this;
    }

    /**
     * @private
     */
    _assertSameSize(b) {
        if (b.columns !== this.columns || b.rows !== this.rows) {
            throw `Expected same size matrices, but got ${this.rows}x${this.columns} and ${b.rows}x${b.columns}.`;
        }
    }

    /**
     * @public
     */
    add(b) {
        this._assertSameSize(b);

        let result = [];

        for (let i = 0; i < this.rows; i++) {
            let row = [];

            for (let j = 0; j < b.columns; j++) {
                row.push(this._data[i][j] + b._data[i][j])
            }

            result.push(row);
        }

        return new Matrix(result);
    }

    /**
     * @public
     */
    min(b) {
        this._assertSameSize(b);

        let result = [];

        for (let i = 0; i < this.rows; i++) {
            let row = [];

            for (let j = 0; j < b.columns; j++) {
                row.push(Math.min(this._data[i][j], b._data[i][j]));
            }

            result.push(row);
        }

        return new Matrix(result);
    }

    /**
     * @public
     */
    max(b) {
        this._assertSameSize(b);

        let result = [];

        for (let i = 0; i < this.rows; i++) {
            let row = [];

            for (let j = 0; j < b.columns; j++) {
                row.push(Math.max(this._data[i][j], b._data[i][j]));
            }

            result.push(row);
        }

        return new Matrix(result);
    }


    /**
     * @private
     */
    _multiplyScalar(factor) {
        let result = [];

        for (let i = 0; i < this.rows; i++) {
            let row = [];

            for (let j = 0; j < this.columns; j++) {
                row.push(this._data[i][j] * factor);
            }

            result.push(row);
        }

        return new Matrix(result);

    }

    /**
     * @public
     */
    multiply(b) {
        if (!isNaN(b)) {
            return this._multiplyScalar(b);
        }

        let result = [];

        if (this.columns !== b.rows) {
            throw `Incompatible dimensions between ${this.columns} columns and ${b.rows} rows.`;
        }

        for (let i = 0; i < this.rows; i++) {
            result.push([]);

            for (let j = 0; j < b.columns; j++) {
                let sum = 0;

                for (let k = 0; k < this.columns; k++) {
                    sum += this._data[i][k] * b._data[k][j];
                }

                result[result.length - 1].push(sum);
            }
        }

        return new Matrix(result);
    }

    /**
     * @public
     */
    convolve(b) {
        const rows = this.rows;
        const columns = this.columns;

        let result = Matrix.zeros(rows, columns);

        const colOffStart = Math.floor(b.columns / 2);
        const rowOffStart = Math.floor(b.rows / 2);

        for (let col=0; col < columns; col++) {
            for (let row=0; row < rows; row++) {

                // Iterate over kernel.
                for (let rowOff=0; rowOff < b.rows; rowOff++) {
                    for (let colOff=0; colOff < b.rows; colOff++) {

                        // Negative, to "flip" rows and columns.
                        const r = rowOffStart - rowOff;
                        const c = colOffStart - colOff;

                        // Check in bounds, crop.
                        if (r < 0 || c < 0) continue;
                        if (r >= rows || c >= columns) continue;

                        // Accumulate.
                        const factor = b._data[rowOff][colOff];
                        result._data[r][c] += factor * this._data[r][c];
                    }
                }
            }
        }

        return result;
    }

    /**
     * @public
     */
    static translationalTransform(v) {
        const row1 = [1, 0, 0, v.x];
        const row2 = [0, 1, 0, v.y];
        const row3 = [0, 0, 1, v.z];
        const row4 = [0, 0, 0, 1];

        return new Matrix([row1, row2, row3, row4]);
    };

    /**
     * @public
     */
    static rotationalTransform(v) {
        // Rotation in the X direction
        let row1 = [1, 0, 0, 0];
        let row2 = [0, Math.cos(v.x), -1*Math.sin(v.x), 0];
        let row3 = [0, Math.sin(v.x), Math.cos(v.x), 0];
        let row4 = [0, 0, 0, 1];
        const Rx = new Matrix([row1, row2, row3, row4]);

        // Rotation in the Y direction
        row1 = [Math.cos(v.y), 0, Math.sin(v.y), 0];
        row2 = [0, 1, 0, 0];
        row3 = [-1*Math.sin(v.y), 0, Math.cos(v.y), 0];
        row4 = [0, 0, 0, 1];
        const Ry = new Matrix([row1, row2, row3, row4]);

        // Rotation in the Z direction
        row1 = [Math.cos(v.z), -1*Math.sin(v.z), 0, 0];
        row2 = [Math.sin(v.z), Math.cos(v.z), 0, 0];
        row3 = [0, 0, 1, 0];
        row4 = [0, 0, 0, 1];
        const Rz = new Matrix([row1, row2, row3, row4]);

        // Total rotation.
        return Rz.multiply(Ry).multiply(Rx);
    };
}

Matrix.MATRICES = {
    IMAGE_PROCESSING: {
        BOX_BLUR: new Matrix([
            [1/9, 1/9, 1/9],
            [1/9, 1/9, 1/9],
            [1/9, 1/9, 1/9],
        ]),
    }
};
