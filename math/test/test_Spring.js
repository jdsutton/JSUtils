import('../Spring.js');
import('../Vector.js');
import('../../util/assert.js');

suite('Sping.js', () => {
    suite('applyForce()', () => {
        spec('Should compress', () => {
            const force = new Vector([0, 1000]);
            const width = 10;
            const height = 100;

            let s = new Spring(width, height);

            s.applyForce(width / 2, 0, force);
            s.update();

            assertTrue(s.getCompressionDistance() > 0);
            assertTrue(s.getLength() < height);
        });

        spec('Should rotate', () => {
            const force = new Vector([1000, 0]);
            const width = 10;
            const height = 100;

            let s = new Spring(width, height);

            s.applyForce(0, 30, force);
            
            assertTrue(s.angularVelocity > 0);
        });
    });
});