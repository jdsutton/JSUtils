import('../KinematicJoints.js');
import('../../util/assert.js');
import('../Vector.js');

// FIXME.
suite('KinematicJoints.js', () => {
    suite('getPointOfRotation()', () => {
        // spec('Should work.', () => {
        //     const base = new Vector(1, 2, 3);
        //     const joints = new KinematicJoints(base.x, base.y, base.z)
        //         .addJoint(10, 0, 0)
        //         .rotateJoint(0, Math.PI / 4)
        //         .addJoint(10, 0, 0)
        //         .rotateJoint(1, Math.PI / 4);

        //     const expected1 = new Vector(
        //         base.x + 7.071067812,
        //         base.y + 7.071067812,
        //         base.z
        //     );
        //     const expected2 = new Vector(
        //         expected1.x,
        //         expected1.y + 10,
        //         base.z
        //     );

        //     const result0 = joints.getPointOfRotation(0);
        //     const result1 = joints.getPointOfRotation(1);
        //     const result2 = joints.getPointOfRotation(2);

        //     assertAlmostEqual(result0.x, base.x);
        //     assertAlmostEqual(result0.y, base.y);
        //     assertAlmostEqual(result0.z, base.z);

        //     assertAlmostEqual(result1.x, expected1.x);
        //     assertAlmostEqual(result1.y, expected1.y);
        //     assertAlmostEqual(result1.z, expected1.z);

        //     assertAlmostEqual(result2.x, expected2.x);
        //     assertAlmostEqual(result2.y, expected2.y);
        //     assertAlmostEqual(result2.z, expected2.z);
        // });

        spec('Should preserve length.', () => {
            const L = 10;
            const joints = new KinematicJoints(0, 0, 0)
                .addJoint(0, L, 0)
                .addJoint(L, 0, 0)
                .addJoint(0, L, 0);

            for (let i = 0; i < 3; i++) {
                joints.rotateJoint(i, Math.random() * Math.PI * 2);
            }

            for (let i = 0; i < 2; i++) {
                const a = joints.getPointOfRotation(i);
                const b = joints.getPointOfRotation(i + 1);

                const dist = Math.sqrt(
                    Math.pow(a.x - b.x, 2)
                    + Math.pow(a.y - b.y, 2)
                );

                assertAlmostEqual(dist, L);
            }
        });
    });

    suite('getTipPosition()', () => {
        spec('Should work.', () => {
            const result = new KinematicJoints(0, 0, 0)
                .addJoint(0, 10, 0)
                .addJoint(0, 10, 0)
                .getTipPosition();

            assertTrue(Math.abs(result.x) < 0.01);
            assertTrue(Math.abs(result.y - 20) < 0.01);
            assertTrue(Math.abs(result.z) < 0.01);
        });

        // spec('Should preserve length.', () => {
        //     const L = 10;
        //     const joints = new KinematicJoints(0, 0, 0)
        //         .addJoint(0, L, 0)
        //         .addJoint(L, 0, 0)
        //         .addJoint(0, L, 0);

        //     for (let i = 0; i < 3; i++) {
        //         joints.rotateJoint(i, Math.random() * Math.PI * 2);
        //     }

        //     for (let i = 0; i < 2; i++) {
        //         const a = joints.getTipPosition(i);
        //         const b = joints.getTipPosition(i + 1);

        //         const dist = Math.sqrt(
        //             Math.pow(a.x - b.x, 2)
        //             + Math.pow(a.y - b.y, 2)
        //         );

        //         assertAlmostEqual(dist, L);
        //     }
        // });
    });

    // spec('moveTipToPosition()', () => {
    //     const target = new Vector(20, 0, 0);
    //     const joints = new KinematicJoints(0, 0, 0)
    //         .addJoint(0, 10, 0)
    //         .addJoint(0, 10, 0)
    //         .moveTipToPosition(target.x, target.y, target.z);
        
    //     const result = joints.getTipPosition();

    //     assertAlmostEqual(result.x, target.x);
    //     assertAlmostEqual(result.y, target.y);
    //     assertAlmostEqual(result.z, target.z);
    // });
});
