import('../CoordinateSystem.js');
import('../Matrix.js');
import('../../util/assert.js');

suite('CoordinateSystem.js', () => {

    spec('computeTransformation()', () => {
        const A = new CoordinateSystem(0, 0, 0);
        const B = new CoordinateSystem(10, 0, 0, 0, 0, Math.PI / 4);

        const point = new Matrix([[0, 0, 0, 1]]).transpose();
        const transformation = CoordinateSystem.computeTransformation(A, B);

        const translationFromBToA = CoordinateSystem
            .computeTranslationTransformation(A, B);
        const rotationFromBToA = CoordinateSystem
            .computeRotationTransformation(A, B);

        let result = point;
        result = translationFromBToA.multiply(result);
        result = rotationFromBToA.multiply(result);
    });
});
