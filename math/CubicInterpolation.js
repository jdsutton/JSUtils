import('Vector.js');

/**
 * @class
 */
class CubicInterpolation {

    /**
     * @constructor
     */
    constructor(startValue, startTime, endValue, endTime) {
        this._startValue = startValue;
        this._startTime = startTime;
        this._endValue = endValue;
        this._endTime = endTime;
        this._timeRange = (this._endTime - this._startTime);
    }

    /**
     * @public
     */
    compute(t) {
        const multiplier = this._s((t - this._startTime) / this._timeRange);

        if (this._startValue instanceof Vector) {
            const scaledRange = this._endValue.subtract(this._startValue).multiply(multiplier);

            return this._startValue.add(scaledRange);   
        }

        return this._startValue + (this._endValue - this._startValue) * multiplier;
    }

    _s(t) {
        t = Math.min(t, 1.0);
        t = Math.max(t, 0.0);

        return Math.pow(t, 3) * (10 + t * (-15 + 6 * t));
    }
}