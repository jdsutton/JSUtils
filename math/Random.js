/**
 * math/Random.js
 */
class Random {

    /**
     * xorshift+
     * @public
     */
    static random() {
        let t = this._seed[0];
        const s = this._seed[1];

        this._seed[0] = s;

        t ^= t << 23;
        t ^= t >> 17;
        t ^= s ^ (s >> 26);

        this._seed[1] = t;

        return (t + s) / 10000 % 1;
    }

    /**
     * @public
     */
    static setSeed(seed) {
        this._seed[0] = 1;
        this._seed[1] = seed;
    }

    /**
     * @public
     */
    static choice(iterable) {
        const arr = Array.from(iterable);
        const index = Math.floor(this.random() * arr.length);

        return arr[index];
    }

    /**
     * @public
     */
    static chooseN(iterable, N) {
        let arr = Array.from(iterable);

        if (N > arr.length) {
            throw new Error(`N (${N}) must be <= number of items (${arr.length})`);
        }


        let result = [];
        let i = 0;

        while (result.length < N) {
            const numNeeded = N - result.length;
            const p = numNeeded / (arr.length - i);

            if (Math.random() < p) {
                result.push(arr[i]);
                arr.slice(i, 1);
            }

            i = (i + 1) % arr.length;
        }

        return result;
    }

    /**
     * Returns a value in the interval [min, max)
     * @public
     */
    static range(min, max) {
        return this.random() * (max - min) + min;
    }

    /**
     * Returns a new shallow copy of the given array with contents in a random order.
     * @public
     */
    static shuffle(array) {
        array = array.slice();

        let currentIndex = array.length;
        let temporaryValue = null;
        let randomIndex = null;

        // While there remain elements to shuffle.
        while (currentIndex > 0) {
            // Pick a remaining element.
            randomIndex = Math.floor(this.range(0, currentIndex));
            currentIndex--;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    /**
     * @public
     */
    static gauss(mu, sigma) {
        mu = mu || 0;
        sigma = sigma || 1;

        let u = 0, v = 0;

        while(u === 0) u = this.random();
        while(v === 0) v = this.random();

        const x = Math.sqrt(-2.0 * Math.log(u))
            * Math.cos(2.0 * Math.PI * v);

        return mu + sigma * x;
    }
}

Random._seed = [
    1,
    Math.random(),
];
