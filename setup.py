#!/usr/bin/env python3

from os import environ
from setuptools import setup, find_packages
from setuptools.command.install import install
from subprocess import check_call as execute

repoDependencies = [
    './Slurp',
]

class PostInstallCommand(install):
    def run(self):
        for repo in repoDependencies:
            target = environ.get('INSTALL_TARGET', None)

            if isinstance(repo, tuple):
                repo = '{}@{}'.format(*repo)
            
            if target is not None:
                print('python3 -m pip install {} --target={}'.format(repo, target))
                execute('python3 -m pip install --upgrade {} --target={}'.format(repo, target), shell=True)
            else:
                execute('python3 -m pip install --upgrade {}'.format(repo).split())

        install.run(self)

setup(name='JSUtils',
    version='1.0',
    description='',
    author='John D. Sutton',
    author_email='',
    url='',
    packages=find_packages(),
    install_requires=[
    ],
    cmdclass={
        'install': PostInstallCommand
    }
)
