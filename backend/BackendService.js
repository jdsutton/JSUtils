import('../util/HTMLString.js');
import('../util/Request.js');

/**
 * @class
 */
class BackendServiceProxyHandler {

    /**
     * @public
     */
    static get(obj, property) {
        if (property in obj) {
            return obj[property];
        }

        return (...args) => {
            let kwargs = args[args.length - 1] || {};
            const a = args.slice(0, args.length - 1);
            const unsafe = Boolean(kwargs.unsafe);
            delete kwargs.unsafe;
            const skipTypecast = Boolean(kwargs.skipTypecast);

            if (obj._dataGetter) {
               Object.assign(kwargs, obj._dataGetter());
            }

            return obj.invoke(property, a, kwargs, unsafe, skipTypecast);
        }
    }
}

/**
 * @class
 */
class BackendService {

    /**
     * @constructor
     */
    constructor(lambdaFunctionName, methodPrefix, dataClass) {
        this._lambdaFunctionName = lambdaFunctionName;
        this._methodPrefix = methodPrefix || 'default';
        this._dataClass = dataClass || null;
        this._dataGetter = null;
        this._breakCache = true;
        this._noTypecastMethods = new Set();
    }

    /**
     * @public
     */
    useCacheBreaking(value) {
        this._breakCache = value;

        return this;
    }

    /**
     * @public
     */
    forAllRequests(callback) {
        this._dataGetter = callback;

        return this;
    }

    /**
     * @public
     */
    noTypecasting(methods) {
        this._noTypecastMethods = new Set(methods);

        return this;
    }

    static declare(lambdaFunctionName, dataClass) {
        const service = new BackendService(lambdaFunctionName, null, dataClass);

        return new Proxy(service, BackendServiceProxyHandler);
    }

    /**
     * @private
     * Escapes an object or list of objects for safety.
     */
    static _escape(obj) {
        if (typeof obj === 'string') {
            return HTMLString.escape(obj);
        } else if (Array.isArray(obj)) {
            return obj.map(item => {
                return this._escape(item);
            });
        } else if (obj !== null && typeof obj === 'object') {
            const keys = Array.from(Object.keys(obj));

            keys.forEach(key => {
                obj[key] = this._escape(obj[key]);
            });

            return obj;
        } else {
            return obj;
        }
    }

    /**
     * @public
     */
    invoke(method, args, kwargs, unsafe, skipTypecast) {
        unsafe = unsafe || false;

        if (this._noTypecastMethods.has(method))
            skipTypecast = true;

        method = this._methodPrefix + '.' + method;

        const cache = new Date().getTime();


        let params = {
            FunctionName: this._lambdaFunctionName,
            Payload: JSON.stringify({
                method,
                args,
                kwargs,
            }),
        };

        if (this._breakCache) params.cache = cache;

        return Request.post(this.constructor._ENDPOINT, params, {}, true).then(response => {
            return this._handleResponse(response, unsafe, skipTypecast);
        });
    }

    /**
     * @private
     */
    _handleResponse(response, unsafe, skipTypecast) {
        const parsed = JSON.parse(response);
        let result;

        if (parsed && parsed.errorMessage !== undefined) {
            return Promise.reject(parsed.errorMessage);
        }

        if (unsafe) {
            result = parsed;
        } else {
            result = this.constructor._escape(parsed);
        }

        if (this._dataClass && !skipTypecast) {
            if (Array.isArray(result)) {
                return result.map(x => new this._dataClass(x, false));
            }

            if (typeof result !== 'object') {
                console.warn('Not an array or object. Do you need to use .noTypecasting() in your backend declaration?');
                console.warn(result);
            }

            return new this._dataClass(result, false);
        }

        return result;
    }
}

BackendService._ENDPOINT = '[["backendEndpoint"]]';

// Handle endpoints like :PORT, use given port on same host.
if (BackendService._ENDPOINT.startsWith(':')) {
    BackendService._ENDPOINT = window.location.protocol + '//' + window.location.hostname
        + BackendService._ENDPOINT;
}