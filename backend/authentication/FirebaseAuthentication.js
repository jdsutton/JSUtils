import { getAuth, signInWithPopup, signInWithRedirect, signOut, GoogleAuthProvider, onAuthStateChanged } from "https://www.gstatic.com/firebasejs/10.7.2/firebase-auth.js";;

/**
 * @class
 */
class FirebaseAuthentication {
    
    /**
     * @public
     * @return {Promise}
     */
    static signInWithPopup(provider) {
        provider = provider || this._PROVIDER.GOOGLE;

        signInWithPopup(this._AUTH, provider).then((result) => {
            this._handleSignInResult(result);
        }).catch((error) => {
            this._handleSignInError(error);
        });
    }

    /**
     * @public
     * @return {Promise}
     */
    static signInWithRedirect(provider) {
        provider = provider || this._PROVIDER.GOOGLE;

        signInWithRedirect(this._AUTH, provider).then((result) => {
            this._handleSignInResult(result);
        }).catch((error) => {
            this._handleSignInError(error);
        });
    }

    /**
     * @private
     */
    static _handleSignInResult(result) {
        // This gives you a Google Access Token. You can use it to access the Google API.
        const credential = GoogleAuthProvider.credentialFromResult(result);
        const token = credential.accessToken;
        // The signed-in user info.
        const user = result.user;
        // IdP data available using getAdditionalUserInfo(result)
        console.debug({
            credential,
            token,
            user,
        });

        this.uid = user.uid;
        this._promiseResolver();
    }

    /**
     * @private
     */
    static _handleSignInError(error) {
        const errorCode = error.code;
        const errorMessage = error.message;

        // The email of the user's account used.
        const email = error.customData.email;

        // The AuthCredential type that was used.
        const credential = GoogleAuthProvider.credentialFromError(error);
        
        console.error({
            errorCode,
            errorMessage,
            email,
            credential,
        });
    }

    /**
     * @public
     * @return {Promise}
     */
    static signOut() {
        return signOut(this._AUTH).then(() => {
            // Sign-out successful.
        }).catch((error) => {
            // An error happened.
        });
    }

    /**
     * @public
     * @return {Boolean}
     */
    static isAuthenticated() {
        return Boolean(this.uid);
    }

    /**
     * @public
     * @return {Promise}
     */
    static waitForSignIn() {
        return this._promise;
    }

    /**
     * @public
     * @return {Promise}
     */
    static waitForSignOut() {
        return this._signOutPromise;
    }

    /**
     * @public
     * @return {Promise<Boolean>}
     */
    static waitForStateChange() {
        return Promise.race([
            this._promise.then(() => true),
            this._signOutPromise.then(() => false),
        ]);
    }

}

FirebaseAuthentication._AUTH = getAuth();
FirebaseAuthentication.uid =  window.localStorage.getItem('FirebaseAuthentication.uid') || null;
FirebaseAuthentication._promise = new Promise((resolve, reject) => {
    FirebaseAuthentication._promiseResolver = resolve;
});

FirebaseAuthentication._signOutPromise = new Promise((resolve, reject) => {
    FirebaseAuthentication._signOutPromiseResolver = resolve;
});

FirebaseAuthentication._PROVIDER = {
    GOOGLE: new GoogleAuthProvider(),
};

onAuthStateChanged(FirebaseAuthentication._AUTH, (user) => {
  if (user) {
    // User is signed in, see docs for a list of available properties
    // https://firebase.google.com/docs/reference/js/auth.user
    FirebaseAuthentication.uid = user.uid;
    window.localStorage.setItem('FirebaseAuthentication.uid', user.id);
    console.log('Signed in');
    FirebaseAuthentication._promiseResolver();
  } else {
    // User is signed out
    console.log('Signed out!');
    window.localStorage.removeItem('FirebaseAuthentication.uid');    

    FirebaseAuthentication._signOutPromiseResolver();
  }
});

window.FirebaseAuthentication = FirebaseAuthentication;

export default FirebaseAuthentication;
