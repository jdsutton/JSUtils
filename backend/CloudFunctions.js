
/**
 * @class
 */
class CloudFunctions {

    /**
     * @public
     */
    static init(uri) {
        this._uri = new URL(uri).toString();
    }

    /**
     * @public
     * @return {Promise}
     */
    static invoke(name, kwargs) {
        kwargs.__cache__ = new Date().getTime();
        
        const uri = QueryString.build(
            this._uri + name + '/',
            kwargs,
        );

        return Request.get(uri);
    }
}

CloudFunctions._uri = null;
