
import('../math/Vector.js');

/**
 * @class
 */
class KDTree {

    /**
     * @constructor
     * @param {Array} items
     * @param {Function} getAxesF - A function returning the dimensional values for a given item.
     * @param {?Number} k - The number of dimensions.
     */
    constructor(items, getAxesF, k) {
        this._items = items;
        this._getAxesF = getAxesF;
        this._k = k || items[0].length;
        if (items.length)
            this._root = this._getNode(items, 0);
        else this._root = null;
    }

    /**
     * @private
     */
    _getNode(items, depth) {
        const axis = depth % this._k;

        const compareF = (a, b) => {
            a = this._getAxesF(a)[axis];
            b = this._getAxesF(b)[axis];

            return a - b;
        }

        const sorted = items.sort(compareF);
        const i = Math.floor(sorted.length / 2);
        const median = sorted[i];
        const left = sorted.slice(0, i);
        const right = sorted.slice(i + 1);
        let leftChild, rightChild;

        if (left.length > 0) {
            leftChild = this._getNode(left, depth + 1);
        } else {
            leftChild = null;
        }

        if (right.length > 0) {
            rightChild = this._getNode(right, depth + 1);
        } else {
            rightChild = null;
        }

        return new KDTree._Node(median, leftChild, rightChild);
    }

    /**
     * @public
     */
    addItem(item) {
        const newNode = new KDTree._Node(item, null, null);
        let depth = 0;
        let node = this._root;

        if (!this._root) {
            this._root = newNode;

            return;
        }

        while (true) {
            const axis = depth % this._k;
            const value = this._getAxesF(item)[axis];
            const value2 = this._getAxesF(node.item)[axis];

            if (value < value2) {
                if (!node.leftChild) {
                    node.leftChild = newNode;

                    return;
                } else {
                    node = node.leftChild;
                }
            } else {
                if (!node.rightChild) {
                    node.rightChild = newNode;

                    return;
                } else {
                    node = node.rightChild;
                }
            }

            depth++;
        }
    }

    /**
     * https://www.cs.cmu.edu/~ckingsf/bioinfo-lectures/kdtrees.pdf
     * https://stackoverflow.com/questions/1627305/nearest-neighbor-k-d-tree-wikipedia-proof
     * @public
     * @param {Array<Number>} point - A point in space.
     */
    nearest(point) {
        let closest = null;
        let minDistance = Infinity;

        for (let item of this._nearestIterator(point)) {
            const node = item[0];
            const distance = item[1];

            if (!closest || distance < minDistance) {
                minDistance = distance;
                closest = node;
            }
        }

        return closest.item;
    }

    /**
     * @private
     */
    _nearestIterator(point) {
        function* makeIterator(root, k, getAxesF) {
            const pointVector = new Vector(point);

            const dist = (axes) => {
                return new Vector(axes)
                    .subtract(pointVector).magnitude();
            };

            let node = root;
            let axes = getAxesF(node.item);
            let depth = 0;

            while (node) {
                const distance = dist(axes);

                yield [node, distance];

                const dim = depth % k;

                if (point[dim] < axes[dim]) {
                    node = node.leftChild;
                    if (node)
                        axes = getAxesF(node.item);
                    depth++;
                } else {
                    node = node.rightChild;
                    if (node)
                        axes = getAxesF(node.item);
                    depth++;
                }
            }
        }

        return makeIterator(this._root, this._k, this._getAxesF);
    }
}

/**
 * @class KDTree._Node
 */
KDTree._Node = class {

    /**
     * @constructor
     */
    constructor(item, leftChild, rightChild) {
        this.item = item;
        this.leftChild = leftChild;
        this.rightChild = rightChild;
    }

    /**
     * @public
     */
    isLeaf() {
        return !(this.leftChild || this.rightChild);
    }
}