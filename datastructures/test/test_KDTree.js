import('../KDTree.js');
import('../../util/assert.js');

suite('KDTree.js', () => {

    spec('nearest()', () => {
        const points = [
            [1, 2],
            [3, 4],
            [5, 6],
        ];

        const tree = new KDTree(points, (p) => p);
        const point = [5.1, 6.2];

        const nearest = tree.nearest(point);

        assertEqual(nearest[0], 5);
        assertEqual(nearest[1], 6);
    });
});
