import('S3.js');

/**
 * @class
 * Handles upload/download to/from S3.
 * @param {String} uploadBucket
 * @param {String} downloadBucket
 * @param {?String} prefix
 */
class S3FileManager {

    /**
     * @constructor
     */
    constructor(uploadBucket, downloadBucket, prefix) {
        /** @private */
        this._uploadBucket = uploadBucket;
        /** @private */
        this._listBucket = downloadBucket;
        /** @private */
        this._prefix = prefix || '';
        if (!this._prefix.endsWith('/')) this._prefix = this._prefix + '/';
        /** @private */
        this._onFilesLoadedCallbacks = [];
        /** @private */
        this._promisesToResolve = [];
        /** @private */
        this._fileList = [];
        this._intervalsByKey = {};

        this._checkFilesLoaded();
    }

    /**
     * @public
     * @param {Function} callback
     * @return {FileUploadComponent}
     */
    onFilesLoaded(callback) {
        this._onFilesLoadedCallbacks.push(callback);

        return this;
    }

    /**
     * @public
     * @param {String} key
     * @param {Function} f - f(s3Object)
     */
    setOnKeyUpdated(key, f) {
        let timestamp = new Date();
        console.debug(`Refrence timestamp ${timestamp}.`);

        this._intervalsByKey[key] = setInterval(() => {
            this._checkKeyUpdated(timestamp, key, f);
        }, this.constructor._FILE_UPDATE_POLL_INTERVAL_MS);
    }

    /**
     * @private
     */
    _checkKeyUpdated(timestamp, key ,f) {
        try {
            S3.getObject(this._listBucket, key, true).then(result => {
                console.debug(`Object loaded with last modified: ${result.lastModified}.`);
                
                if (result.lastModified > timestamp) {
                    timestamp = result.lastModified;

                    f(result);
                    clearInterval(this._intervalsByKey[key]);
                }
            });
        } catch (e) {
            // Key does not exist yet.
            console.warn(e);
        }
    }

    /**
     * @public
     * @param {String} key
     */
    hasFile(key) {
        let found = false;
        key = S3FileManager.basename(key);

        this._fileList.forEach(file => {
            found = found || (S3FileManager.basename(file.name) == key);
        });

        return found;
    }

    /**
     * @public
     * @param {Element} element - The file upload element.
     * @param {?String} key
     * @return {Promise<{href, name}[]>}
     */
    upload(element, key) {
        return S3.uploadFile(element, this._uploadBucket, this._prefix + key).then(() => {
            element.value = null;
            const promise = new Promise((resolve, reject) => {
                this._promisesToResolve.push(resolve);
            });

            this._refreshUntilFilesLoaded();

            return promise;
        });
    }

    _refreshUntilFilesLoaded() {
        this._refreshListInterval = setInterval(() => {
            this._checkFilesLoaded();
        }, this.constructor._refreshListTimeoutMillis);
    }

    /**
     * @private
     */
    _listFiles() {
        return S3.listFiles(this._listBucket, this._prefix);
    }

    _checkFilesLoaded() {
        this._listFiles().then(files => {
            // TODO: More in-depth equality check.
            if (!this._fileList || files.length !== this._fileList.length) {
                clearInterval(this._refreshListInterval);
                this._refreshListInterval = null;

                this._fileList = files;

                this._onFilesLoadedCallbacks.forEach(callback => {
                    callback(files);
                });
                this._promisesToResolve.forEach(resolve => {
                    resolve(files);
                });
            }
        });
    }

    /**
     * @public
     * @return {Promise<{href, name}[]>}
     */
    getFileList() {
        return this._listFiles().then(files => {
            this._fileList = files;

            return files || [];
        });
    }

    /**
     * @public
     * @param {String} path
     * @return {String}
     */
    static basename(path) {
        path = path.split('/');

        return path[path.length - 1];
    }
}

S3FileManager._FILE_UPDATE_POLL_INTERVAL_MS = 2000;