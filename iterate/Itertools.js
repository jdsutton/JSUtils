/**
 * @class
 */
class Itertools {

    /**
     * @public
     */
    static zip(a, b) {
        let result = [];
        let i = 0;

        while (i < a.length && i < b.length) {
            result.push([a[i], b[i]]);
            i++;
        }

        return result;
    }
}