/**
 * @class
 */
class PaginatedPromiseIterator {

    /**
     * @public
     * @constructor
     */
    constructor(loadF, n) {
        this._N = n || 16;
        this._i = 0;
        this._loadF = loadF;
        this._forEachResolver = null;
    }

    /**
     * @public
     */
    forEach(callback) {
        this._i = 0;

        this._invoke(callback);

        return new Promise((resolve, reject) => {
            this._forEachResolver = resolve;
        });
    }

    /**
     * @private
     */
    _invoke(callback) {
        this._loadF(this._N, this._i).then(result => {
            let promises = [];
            
            this._i += result.length;

            result.forEach(item => {
                promises.push(callback(item));
            });

            const doNext = () => {
                if (result.length === this._N) {
                    // More data.
                    this._invoke(callback);
                } else {
                    // No more data.
                    this._forEachResolver();
                    this._forEachResolver = null;
                }
            }

            Promise.all(promises).then(() => {
                doNext();
            }).catch(err => {
                doNext();
            });
        });
    }
}