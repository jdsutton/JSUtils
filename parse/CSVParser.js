/**
 * @class
 */
class CSVParser {

    /**
     * @constructor
     */
    constructor(text, separator, quote) {
        this._lines = text.split('\n');
        this._index = 0;
        this._separator = separator || ',';
        this._quote = quote;

        // Infer quote character.
        if (!this._quote) {
            for (let i = 0; i < text.length; i++) {
                if (CSVParser._QUOTES.indexOf(text[i]) > -1) {
                    this._quote = text[i];

                    break;
                }
            }
        }
        // TODO: Infer separator.

        this[Symbol.iterator] = () => this;
    }

    /**
     * @public
     */
    next() {
        const line = this._lines[this._index];
        let items = [];
        let inQuote = false;
        let startIndex = 0;

        if (line === undefined) {
            return {
                value: this._index,
                done: true,
            };
        }

        for (let i = 0; i < line.length; i++) {
            if (line[i] === this._quote) {
                inQuote = !inQuote;
            } else if ((i === line.length - 1 || line[i] === this._separator) && !inQuote) {
                // End of item.
                let item = line.slice(startIndex, i);
                
                if (item.startsWith(this._quote)) {
                    item = item.slice(1, item.length);
                }
                if (item.endsWith(this._quote) || item.endsWith(this._separator)) {
                    item = item.slice(0, item.length - 1);
                }

                items.push(item);
                startIndex = i + 1;
            }
        }

        this._index++;

        return {
            value: items,
            done: false,
        };
    }
}

CSVParser._QUOTES = [
    '"', '\'', '`', '\u0022', '\u2E42', '\uFF02',
];