/**
 * @class
 */
class Path {
    
    /**
     * @public
     */
    static basename(path) {
        const parts = path.split(/[\/\\]/);

        return parts[parts.length - 1];
    }
}