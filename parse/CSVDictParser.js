import('CSVParser.js');

/**
 * @class
 */
class CSVDictParser extends CSVParser {

    /**
     * @constructor
     */
    constructor() {
        super(...arguments);

        this._header = super.next().value;
    }

    /**
     * @public
     */
    next() {
        let result = super.next();
        let value = result.value;
        let newValue = {};

        for (let i in value) {
            newValue[this._header[i]] = value[i];
        }

        result.value = newValue;

        return result;
    }
}