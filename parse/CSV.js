/**
 * @public
 */
class CSV {

    /**
     * @public
     */
    static export(rows, fileName) {
        fileName = fileName || 'data';

        const csvContent = "data:text/csv;charset=utf-8," 
            + rows.map(e => e.join(",")).join("\n");
        const uri = encodeURI(csvContent);

        let downloadLink = document.createElement('a');
        downloadLink.href = uri;
        downloadLink.download = fileName + '.csv';

        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
    }
}