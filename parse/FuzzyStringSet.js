import('JSString.js');

/**
 * @class
 */
class FuzzyStringSet {
	
    /**
     * constructor
     */
    constructor(strings) {
        this._strings = new Set(strings
            .map(x => FuzzyStringSet._cleanQuery(x)));
    }

    /**
     * @private
     */
    static _cleanQuery(query, synonyms) {
        // Remove non-letters, make all lowercase.
        let result = query.replace(/[^\w\s]/g, '').toLowerCase();

        // Collapse characters repeated > 2 times in a row.
        for (let i = 0; i < result.length - 3; i++) {
            while (result[i] === result[i + 1] && result[i + 1] == result[i + 2]) {
                result = result.slice(0, i) + result.slice(i + 1, result.length);
            }
        }

        if (synonyms)
            result = this._reduceSynonyms(result, synonyms);

        return result;
    }

    /**
     * @public
     */
    getClosestMatch(query) {
        let closestMatch = null;
        let minDistance = Infinity;
        const cleanQuery = this.constructor._cleanQuery(query);

        if (this._strings.has(cleanQuery)) return cleanQuery;

        this._strings.forEach(knownQuery => {
            const distance = JSString.editDistance(cleanQuery, knownQuery);

            if (distance < minDistance) {
                minDistance = distance;
                closestMatch = knownQuery;
            }
        });

        return closestMatch;
    }

    /**
     * @public
     */
    getClosestMatches(query) {
        let distances = {};
        let strings = Array.from(this._strings);

        strings.forEach((s) => {
            distances[s] = JSString.editDistance(s, query);
        });

        strings.sort((a, b) => {
            return distances[a] - distances[b];
        });

        return strings;
    }

}