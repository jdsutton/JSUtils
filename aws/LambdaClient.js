import('../util/Request.js');

/**
 * Contains logic for interacting with lambda servers.
 * @param {?String} uri - Optional lambda server endpoint.
 * @param {?Number} port - Optional port corresponding to endpoint. 
 */
class LambdaClient {
	
    /**
     * @constructor
     */
    constructor(uri, port) {
        AWS.config.update({region: 'us-east-1'});
        AWS.config.credentials = new AWS.CognitoIdentityCredentials({
            IdentityPoolId: 'us-east-1:4edde0a7-205e-47c0-859e-fbc026671cdf'
        });

        this._env = '[["env"]]';
        this._uri = uri;
        this._port = port;
        this._client = this._getClient();
    }

    /**
     * @private
     */
    _getClient(endpoint) {
        // https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Lambda.html

        return new AWS.Lambda({
            region: 'us-east-1',
            apiVersion: '2015-03-31',
            endpoint: endpoint,
        });
    }

    /**
     * @private
     */
    _invoke(params, callback) {
        if (this._uri && this._port) {
            return Request.post(this._uri + ':' + this._port, params, {}, true)
                .then(result => {
                    result = {'Payload': result};
                    callback(null, result);
                }, err => {
                    callback(err, null);
                });
        }

        return this._client.invoke(params, callback);
    }

    /**
     * Calls a Lambda function by name.
     * @public
     * @param {String} name - The name of the Lambda function.
     * @param {Object<String, Object>} params - A dict of parameters to pass to the Lambda.
     * @return {Promise<Object>} - The value returned from the Lambda.
     */
    call(name, params) {
        if (this._env === 'staging') {
            name = name + '_staging';
        }
        
        params.env = this._env;

        const pullParams = {
            FunctionName: name,
            InvocationType: 'RequestResponse',
            LogType: 'None',
            Payload: JSON.stringify(params)
        };

        return new Promise((resolve, reject) => {
            this._invoke(pullParams, (error, data) => {
                if (error) {
                    reject(error);
                }
                if (!data.Payload) {
                    reject('Lambda returned empty payload. Ensure your lambda is returning a value');
                }
                else {
                    const result = JSON.parse(data.Payload);
                    
                    if (result && result.errorMessage) {
                        reject(result);
                    }
                    else {
                        resolve(result);
                    }
                }
            });
        });   
    }
}
