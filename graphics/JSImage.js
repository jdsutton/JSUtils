import('../util/Dom.js');

/**
 * @class
 */
class JSImage {

    /**
     * @constructor
     */
    constructor(imageSrc) {
        const img = document.createElement('img');
        this._canvas = document.createElement('canvas');
        this._loadPromise = new Promise((resolve, reject) => {
            this._loadResolver = resolve;
        });

        img.onload = () => {
            this.width = img.naturalWidth;
            this.height = img.naturalHeight;

            this._canvas.setAttribute('width', `${this.width}px`);
            this._canvas.setAttribute('height', `${this.height}px`);
            this._canvas.getContext('2d').drawImage(img, 0, 0);

            this._loadResolver();
        };

        img.src = imageSrc;

        this._img = img;
    }

    /**
     * @public
     */
    get src() {
        return this._img.src;
    }

    /**
     * @public
     * @return {Promise}
     */
    load() {
        return this._loadPromise;
    }

    /**
     * @public
     */
    getPixels() {
        return this._canvas.getContext('2d').getImageData(0, 0, this.width, this.height).data;
    }

    /**
     * @public
     */
    getPixel(x, y) {
        return this._canvas.getContext('2d').getImageData(x, y, 1, 1).data;
    }

    /**
     * @public
     */
    getIntensity(x, y, ignoreAlpha) {
        const pixel = this.getPixel(x, y);
        let total = 0.0;
        let count = 0;

        for (let i = 0; i < pixel.length; i++) {
            if (i > 2 && ignoreAlpha) break;

            total += pixel[i];
            count++;
        }

        return total / (count * 255);
    }
}
