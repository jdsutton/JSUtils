

/**
 * @class
 */
class Shapes {

    /**
     * @public
     */
    constructor(context) {
        this._context = context;
        this._fill = null;
        this._stroke = null;
        this._lineHeight = 12;

        this._updateLineHeight();

        return this;
    }

    /**
     * @private
     */
    static _hexToRgbA(hex){
        let arr = this._hexToRgbAArray(hex);

        return 'rgba(' + arr.join(',') + ')';
    }

    /**
     * @private
     */
    static _hexToRgbAArray(hex) {
        let c;

        c = hex.substring(1).split('');

        if (c.length === 6) {
            c.push('F');
            c.push('F');
        }

        if(c.length === 3){
            c = [c[0], c[0], c[1], c[1], c[2], c[2], 'F', 'F'];
        }
        c = '0x'+c.join('');

        return [(c>>24)&255, (c>>16)&255, (c>>8)&255, (c&255) / 255];
    }

    /**
     * @public
     */
    setAlpha(alpha) {
        this._context.globalAlpha = alpha;
    }

    /**
     * @public
     */
    push() {
        this._context.save();
    }

    /**
     * @public
     */
    pop() {
        this._context.restore();
    }

    /**
     * @public
     */
    scale(x, y) {
        if (y === undefined) y = x;

        this._context.scale(x, y);
    }

    /**
     * @public
     */
    translate(x, y) {
        this._context.translate(x, y);
    }

    /**
     * @public
     */
    rotate(theta) {
        this._context.rotate(theta);
    }

    /**
     * @public
     */
    clearCanvas() {
        this._context.clearRect(
            0,
            0,
            this._context.canvas.width,
            this._context.canvas.height,
        );

        return this;
    }

    /**
     * @private
     */
    static _fixColor(color, color2, color3, color4) {
        if (color4 === undefined) color4 = 255;
        
        if (!isNaN(color)) {
            if (isNaN(color2)) {
                color = `rgb(${color}, ${color}, ${color})`;
            } else {
                color = `rgba(${color}, ${color2}, ${color3}, ${color4})`;
            }
        }

        if (color.startsWith('#'))
            color = Shapes._hexToRgbA(color);

        return color;
    }

    /**
     * @public
     */
    static lerpColors(value, minV, maxV, colors) {
        let result;

        colors = Array.from(colors);

        for (let i in colors) {
            colors[i] = this._hexToRgbAArray(colors[i]);
        }

        const increment = 1 / (colors.length - 1);
        value = (value - minV) / (maxV - minV);
        let i = Math.floor(value / increment);

        if (i >= colors.length - 1) {
            result = colors[colors.length - 1];
        } else {
            const color0 = colors[i];
            const color1 = colors[i + 1];
            value = (value % increment) / increment;

            result = [0, 0, 0, 0];

            for (let j = 0; j < result.length; j++) {
                const diff = color1[j] - color0[j];

                result[j] = color0[j] + diff * value;

                if (j < 3) {
                    result[j] = Math.round(result[j]);
                }
            }
        }

        return 'rgba(' + result.join(',') + ')';
    }

    /**
     * @public
     */
    fill(color, color2, color3, color4) {
        color = Shapes._fixColor(color, color2, color3, color4);

        this._fill = color;
        this._context.fillStyle = color;

        return this;
    }

    /**
     * @public
     */
    setFillStyle(fill) {
        this._fill = fill;
        this._context.fillStyle = fill;
    }

    /**
     * @public
     */
    noFill() {
        this._fill = null;
        this._context.fillStyle = null;

        return this;
    }

    /**
     * @public
     */
    stroke(color1, color2, color3) {
        const color = Shapes._fixColor(color1, color2, color3);

        this._stroke = color;
        this._context.strokeStyle = color;

        return this;
    }

    /**
     * @public
     */
    noStroke() {
        this._stroke = null;
        this._context.strokeStyle = null;

        return this;
    }

    /**
     * @public
     */
    strokeWeight(weight) {
        this._context.lineWidth = weight;

        return this;
    }

    /**
     * @public
     */
    rect(x, y, w, h) {
        this._context.beginPath();
        this._context.rect(x, y, w, h);
        if (this._fill) this._context.fill();
        if (this._stroke) this._context.stroke();

        return this;
    }

    /**
     * @public
     */
    ellipse(x, y, w, h) {
        this._context.beginPath();

        this._context.ellipse(x, y, w/2, h/2, 0, 0, Math.PI*2)

        if (this._fill) this._context.fill();
        if (this._stroke) this._context.stroke();

        return this;
    }

    /**
     * @public
     */
    arc(x, y, radius, theta0, theta1, open) {
        this._context.beginPath();

        if (open) {
            this._context.moveTo(x, y);
        }

        this._context.arc(x, y, radius, theta0, theta1);

        if (open) {
            this._context.lineTo(x, y);
        }

        if (this._fill) this._context.fill();
        if (this._stroke) this._context.stroke();

        this._context.closePath();
    }

    /**
     * @public
     */
    background(color, color2, color3, color4) {
        this.fill(color, color2, color3, color4);
        this.noStroke();

        this.rect(0, 0, this.maxWidth(), this.maxHeight());
    }

    /**
     * @public
     */
    line(x, y, x2, y2) {
        if (!this._stroke) return;

        this._context.beginPath();
        this._context.moveTo(x, y);
        this._context.lineTo(x2, y2);
        this._context.stroke();

        return this;
    }

    /**
     * @public
     */
    lineCap(cap) {
        this._context.lineCap = cap;
    }

    /**
     * @public
     */
    font(f) {
        this._context.font = f;

        this._updateLineHeight();
    }

    /**
     * @private
     */
    _updateLineHeight() {
        this._lineHeight = parseInt(this._context.font
            .replace('bold ', '')
            .replace('italic ', '')
            .split(' ')[0]
            .replace('px', '')) * 1.5;
    }

    /**
     * @public
     */
    text(s, x, y, maxWidth) {
        if (!s) return;

        if (!maxWidth) {
            if (this._stroke !== null) this._context.strokeText(s, x, y);
            this._context.fillText(s, x, y);

            return this;
        }

        const words = s.split(' ');
        let line = '';
        let yoff = 0;

        for (let i in words) {
            const word = words[i];
            const width = this._context.measureText(line + word + ' ').width;

            if (width > maxWidth && i > 0) {
                if (this._stroke !== null) this._context.strokeText(line, x, y + yoff);
                this._context.fillText(line, x, y + yoff);

                line = word + ' ';
                yoff += this._lineHeight;
            } else {
                line = line + word + ' ';
            }
        }

        if (this._stroke !== null) this._context.strokeText(line, x, y + yoff);
        this._context.fillText(line, x, y + yoff);

        return this;
    }

    /**
     * @public
     */
    textSize(size) {
        this._context.font = size + 'px ' + this._context.font.split(' ').slice(1);

        this._updateLineHeight();
    }

    /**
     * @public
     */
    textAlign(xAlign, yAlign) {
        this._context.textAlign = xAlign;

        if (yAlign) {
            this._context.textBaseline = {
                'center': 'middle',
            }[yAlign];
        }
    }

    /**
     * @public
     */
    triangle(x1, y1, x2, y2, x3, y3) {
        this._context.beginPath();
        this._context.moveTo(x1, y1);
        this._context.lineTo(x2, y2);
        this._context.lineTo(x3, y3);
        this._context.lineTo(x1, y1);
        this._context.closePath();
        if (this._fill) this._context.fill();
        if (this._stroke) this._context.stroke();
    }

    /**
     * @public
     */
    quad(x1, y1, x2, y2, x3, y3, x4, y4) {
        this._context.beginPath();
        this._context.moveTo(x1, y1);
        this._context.lineTo(x2, y2);
        this._context.lineTo(x3, y3);
        this._context.lineTo(x4, y4);
        this._context.lineTo(x1, y1);
        this._context.closePath();
        if (this._fill) this._context.fill();
        if (this._stroke) this._context.stroke();
    }

    /**
     * @public
     */
    polygon(points) {
        this._context.beginPath();
        this._context.moveTo(points[0][0], points[0][1]);

        for (let i = 1; i < points.length; i++) {
            this._context.lineTo(points[i][0], points[i][1]);
        }

        this._context.lineTo(points[0][0], points[0][1])

        this._context.closePath();
        if (this._fill) this._context.fill();
        if (this._stroke) this._context.stroke();
    }

    /**
     * @public
     */
    regularPolygon(x, y, radius, npoints) {
        const dTheta = Math.PI * 2 / npoints;
        this._context.beginPath();
        this._context.moveTo(x + radius, y);

        for (let theta = dTheta; theta < (Math.PI * 2); theta += dTheta) {
            const px = x + Math.cos(theta) * radius;
            const py = y + Math.sin(theta) * radius;

            this._context.lineTo(px, py);
        }

        this._context.lineTo(x + radius, y);

        this._context.closePath();
        if (this._fill) this._context.fill();
        if (this._stroke) this._context.stroke();
    }

    /**
     * @public
     */
    bezier(x, y, x2, y2, cx1, cy1, cx2, cy2) {
        this._context.beginPath();
        this._context.moveTo(x, y);
        this._context.bezierCurveTo(x2, y2, cx1, cy1, cx2, cy2);
        this._context.closePath();

        if (this._fill) this._context.fill();
        if (this._stroke) this._context.stroke();
    }

    /**
     * @public
     */
    drawImage(img, x, y, width, height) {
        width = width || img.width;
        height = height || img.height;

        this._context.drawImage(img, x, y, width, height);
    }

    /**
     * @public
     */
    setCanvasSize(width, height) {
        this._context.canvas.width = width;
        this._context.canvas.height = height;

        return this;
    }

    /**
     * @public
     */
    maxHeight() {
        return this._context.canvas.height;
    }

    /**
     * @public
     */
    maxWidth() {
        return this._context.canvas.width;
    }

    /**
     * @public
     */
    scalePixelized() {
        this._context.webkitImageSmoothingEnabled = false;
        this._context.mozImageSmoothingEnabled = false;
        this._context.imageSmoothingEnabled = false;
    }
}

Shapes.ALIGN = {
    LEFT: 'left',
    CENTER: 'center',
};
