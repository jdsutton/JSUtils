
/**
 * @class
 * Handles navigation for Single-Page Apps.
 */
class SPANavigator {

    /**
     * @public
     * @param {String} url
     * @param {Function} callback
     */
    static onNavigateTo(url, callback) {
        if (!this._callbacks[url]) {
            this._callbacks[url] = [];
        }

        this._callbacks[url].push(callback);
    }

    /**
     * @public
     * "Navigate" to a given URL.
     * @param {String} url
     */
     static navigateTo(url, event) {
         if (event) {
             event.preventDefault();
             event.stopPropagation();
         }

         // Set URL and update history.
         window.history.pushState(
             {},
             url,
             window.location.origin + url,
         );

         this._invokeCallbacks(url);
     }

     /**
      * @public
      * Handle initialization of page.
      * Parse current URL and call appropriate callbacks.
      */
     static init() {
         const url = window.location.pathname;

        this._invokeCallbacks(url);
     }

    /**
     * @private
     * @param {String} url
     * @param {String} pattern
     */
    static _urlMatchesPattern(url, pattern) {
        const parts = url.split('/').filter((x) => x.length);
        const patternParts = pattern.split('/').filter((x) => x.length);

        let i;
        for (i = 0; i < parts.length; i++) {
            if (patternParts[i] == '**') return true;

            if (i >= patternParts.length) return false;
            if (parts[i] != patternParts[i]
                && patternParts[i] != '*') return false;
        }

        return i >= patternParts.length;
    }

    /**
     * @private
     * param {String} url
     */
    static _getCallbacks(url) {
        let result = [];

        for (let key in this._callbacks) {
            if (this._urlMatchesPattern(url, key)) {
                result = result.concat(this._callbacks[key]);
            }
        }

        return result;
    }

     /**
      * @private
      * @param {String} url
      */
     static _invokeCallbacks(url) {
         const callbacks = this._getCallbacks(url);

         if (callbacks) {
             callbacks.forEach((c) => c(url));
         }
     }
}

SPANavigator._callbacks = {};

window.onpopstate = () => {
    SPANavigator._invokeCallbacks(window.location.pathname);
};
