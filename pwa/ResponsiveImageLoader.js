
import('../graphics/JSImage.js');

/**
 * @class
 */
class ResponsiveImageLoader {

    /**
     * @public
     */
    static load(element, imageURIs) {
        let promises = [];
        let lastImageSize = 0;

        for (let uri of imageURIs) {
            const img = new JSImage(uri);

            promises.push(
                img.load().then(() => {
                    return img;
                }),
            );
        }

        Promise.all(promises);

        for (let i=0; i < promises.length; i++) {
            promises[i].then((img) => {
                const size = img.width * img.height;

                // If bigger, update.
                if (size > lastImageSize) {
                    lastImageSize = size;
                    element.src = img.src;
                }
            });
        }
    }
}
