/**
 * @class
 */
class Touch {

    /**
     * @constructor
     */
    constructor(element, preventDefault) {
        this._element = element || document.body;
        this._px = 0;
        this._py = 0;
        this.x = 0;
        this.y = 0;
        this._onTouchStartCallbacks = [];
        this._onTouchEndCallbacks = [];
        this._onTouchMoveCallbacks = []

        element.addEventListener(
            'touchstart',
            e => {
                if (preventDefault)
                    e.preventDefault();

                this._onTouchStart(e);

                return !preventDefault;
            }
        );

        element.addEventListener(
            'touchmove',
            e => {
                if (preventDefault)
                    e.preventDefault();

                this._onTouchMove(e);

                return !preventDefault;
            }
        );

        element.addEventListener(
            'touchend',
            e => {
                if (preventDefault)
                    e.preventDefault();

                this._onTouchEnd(e);

                return !preventDefault;
            }
        );
    }


    /**
     * @private
     */
    _updatePosition(event) {
        const offset = this._element.getBoundingClientRect();
        const touch = event.targetTouches[0];

        this._px = this.x;
        this._py = this.y;

        this.x = touch.clientX - offset.left;
        this.y = touch.clientY - offset.top;
    }

    /**
     * @private
     */
    _onTouchStart(event) {
        Touch._event = event;
        this._updatePosition(event);

        this._onTouchStartCallbacks.forEach(f => f(event));
    }

    /**
     * @private
     */
    _onTouchMove(event) {
        Touch._event = event;
        this._updatePosition(event);

        this._onTouchMoveCallbacks.forEach(f => f(event, this.dx, this.dy));
    }

    /**
     * @private
     */
    _onTouchEnd(event) {
        Touch._event = event;
        this._onTouchEndCallbacks.forEach(f => f(event));
    }

    /**
     * @public
     */
    onStart(callback) {
        this._onTouchStartCallbacks.push(callback);

        return this;
    }

    /**
     * @public
     */
    static onStart(callback) {
        this._onTouchStartCallbacks.push(callback);

        return this;
    }

    /**
     * @public
     */
    onMove(callback) {
        this._onTouchMoveCallbacks.push(callback);

        return this;
    }

    /**
     * @public
     */
    static onMove(callback) {
        this._onTouchMoveCallbacks.push(callback);

        return this;
    }

    /**
     * @public
     */
    onEnd(callback) {
        this._onTouchEndCallbacks.push(callback);

        return this;
    }

    /**
     * @public
     */
    static onEnd(callback) {
        this._onTouchEndCallbacks.push(callback);

        return this;
    }

    /**
     * @public
     */
    get dx() {
        return this.x - this._px;
    }

    /**
     * @public
     */
    get dy() {
        return this.y - this._py;
    }

    /**
     * @public
     */
    getPositionRelativeToElement(event, element) {
        return Touch.getPositionRelativeToElement(event, element);
    }

    /**
     * @public
     */
    static getPositionRelativeToElement(event, element) {
        event = event || this._event;

        const offset = element.getBoundingClientRect();
        const touch = event.changedTouches[0];

        return {
            x: touch.clientX - offset.left,
            y: touch.clientY - offset.top,
        };
    }

}

Touch._event = null;
Touch._onTouchStartCallbacks = [];
Touch._onTouchMoveCallbacks = [];
Touch._onTouchEndCallbacks = [];
