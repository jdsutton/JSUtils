import('../util/Page.js');

/**
 * @class
 */
class Mouse {

    /**
     * @public
     */
    static isDown() {
        return this._isDown;
    }

    /**
     * @public
     */
    static getPositionRelativeToElement(event, element) {
        const offset = element.getBoundingClientRect();
        event = event || this._event;

        return {
            x: event.clientX - offset.left,
            y: event.clientY - offset.top
        }
    }

    /**
     * @public
     */
    static getPositionOnPage(event) {
        event = event || this._event;
        
        return {
            x: event.pageX,
            y: event.pageY,
        }
    }

    /**
     * @public
     */
    static onPress(callback) {
        this._onPress.push(callback);
    }

    /**
     * @public
     */
    static onRelease(callback) {
        this._onRelease.push(callback);
    }

    /**
     * @public
     */
    static onMove(callback) {
        this._onMove.push(callback);
    }

    /**
     * @public
     * @return {Number}
     * The change in the x position of the mouse since last update.
     */
    static get dx() {
        return this._x - this._px;
    }

    /**
     * @public
     * @return {Number}
     * The change in the y position of the mouse since last update.
     */
    static get dy() {
        return this._y - this._py;
    }

    /**
     * @public
     */
    static isRightClick(event) {
        return event.which === 3 || event.button === 2;
    }

    /**
     * @public
     */
    static buttonIsPressed(button) {
        return Boolean(this._buttonStates[button]);
    }

    /**
     * @public
     */
    static onScroll(callback) {
        this._onScroll.push(callback);
    }

    /**
     * @public
     */
    static isRightClick(event) {
        if (event.which === 3) return true;
        if (event.button === 2) return true;

        return false;
    }
}

Mouse._onRelease = [];
Mouse._onPress= [];
Mouse._onMove = [];
Mouse._onScroll = [];
Mouse._buttonStates = {};
Mouse._px = 0;
Mouse._py = 0;
Mouse._x = 0;
Mouse._y = 0;
Mouse._event = null;

Page.addLoadEvent(() => {
    document.body.onmousewheel = event => {
        Mouse._onScroll.forEach(callback => {
            callback(Math.sign(event.deltaY));
        });
    };

    document.body.onmousedown = (event) => {
        Mouse._buttonStates[event.button] = true;
        Mouse._event = event;
        Mouse._isDown = true;
        Mouse._onPress.forEach(callback => {
            callback(event);
        });
    };

    document.body.onmouseup = (event) => {
        Mouse._buttonStates[event.button] = false;
        Mouse._event = event;
        Mouse._isDown = false;
        Mouse._onRelease.forEach(callback => {
            callback(event);
        });
    };

    document.onmousemove = (event) => {
        Mouse._event = event;
        Mouse._px = Mouse._x;
        Mouse._py = Mouse._y;

        const pos = Mouse.getPositionOnPage();
        Mouse._x = pos.x;
        Mouse._y = pos.y;

        Mouse._onMove.forEach(callback => {
            callback(event, Mouse.dx, Mouse.dy);
        });
    };
});