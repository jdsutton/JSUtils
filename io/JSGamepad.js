/**
 * @class
 */
class JSGamepad {
    
    /**
     * @public
     */
    static list() {
        if (navigator.getGamepads()) return Array.from(navigator.getGamepads());
        if (navigator.webkitGetGamepads()) return Array.from(navigator.webkitGetGamepads());

        return [];
    }
}