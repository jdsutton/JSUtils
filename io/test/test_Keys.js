let document = {};

import('../../util/assert.js');
import('../Keys.js');

suite('Keys.js', function() {
    suite('#equals()', function() {
        spec('Number and name should match', function() {
            assertTrue(Key.equals(13, KEYS.ENTER_KEY));
            assertTrue(Key.equals('Enter', KEYS.ENTER_KEY));
        });
    });
});