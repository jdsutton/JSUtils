/**
 * @class
 */
class SelfDestructMessage {

    /**
     * @public
     */
    static create(msg, timeToLiveMs) {
        if (timeToLiveMs === undefined) timeToLiveMs = 2000;
        const id = '_selfDestructMessage_' + (new Date()).getTime();

        setTimeout(() => {
            const element = document.getElementById(id);
            element.parentElement.removeChild(element);
        }, timeToLiveMs);

        const result = document.createElement('div');
        result.id = id;
        result.innerHTML = msg;
        Dom.addClass(result, 'SelfDestructMessage');
        
        return result;
    }
}