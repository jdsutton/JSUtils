import('/src/JSUtils/components/Component.js');

/**
 * @class
 * A clickable button.
 */
class ButtonComponent extends Component {

    /**
     * @constructor
     * @param {String} label
     */
    constructor(label) {
        const params = {label};
        
        super(ButtonComponent.ID.TEMPLATE.THIS, params);
        
        this._onClickCallbacks = [];
    }

    /**
     * Calls all onClick callbacks set by the setOnClick() method.
     * @public
     */
    onClick() {
        this._onClickCallbacks.forEach(callback => {
            callback();
        });
    }

    /**
     * Sets up a new callback to be called when the button is clicked.
     * @public
     * @param {Function} callback
     */
    setOnClick(callback) {
        this._onClickCallbacks.push(callback);

        return this;
    }

    /**
     * Controls the enabled/disabled state of the button.
     * @public
     * @param {Boolean} value
     */
    setDisabled(value) {
        this._params.disabled = value;

        this.update();

        return this;
    }
}

ButtonComponent.ID = {
    TEMPLATE: {
        THIS: 'ButtonComponent_template',
    },
};
