[[InfiniteScrollList.js]]
[[TableComponent/driver.js]]

/**
 * @class
 */
class InfiniteScrollTable {
    /**
     * @constructor
     * @param {Table} table - Table object specifying the layout and rendering details.
     * @param {String} tableId - Id of a table element to use.
     * @param {String?} listContainerId - Id of the element to render rows into. If null, will default to the first <tbody> inside the <table>
     * @param {String} scrollContainerId - Id of the scrolling element in/around the table.
     * @param {Function} loadF - Function which loads more rows of the form f(offset, limit) => Promise<Object[]>.
     */
    constructor(table, tableId, listContainerId, scrollContainerId, loadF,
        sortableColumnClass) {
        table = table.copy();
        this.table = table.setId(tableId);
        this.tableId = tableId;
        this.listContainerId = listContainerId;
        this.scrollContainerId = scrollContainerId;

        const tableElement = Dom.getById(this.tableId);

        if (!this.listContainerId) {
            const tbody = document.createElement('tbody');
            Dom.appendTo(tableElement, tbody);

            this.listContainerId = tableElement.tBodies[0];
        }

        // Table header.
        sortableColumnClass = sortableColumnClass || 'Clickable';
        this.setSortableColumnClass(sortableColumnClass);
        Dom.setContents(this.listContainerId, table.renderHeader());

        this.table.setOnReorder(() => {
            return this._onTableReorder()
        });

        this.list = new InfiniteScrollList(
            this.scrollContainerId,
            this.listContainerId,
            row => this.table.renderRow(row),
            loadF
        );

        this.list.loadMore();
    }

    /**
     * @public
     */
    setFilter(f) {
        this.list.setFilter(f);

        return this;
    }

    /**
     * @public
     */
    stop() {
        this.list.stop();
    }

    /**
     * @public
     */
    reset() {
        this.table.resetOrder();
        this.list.reset();
        Dom.setContents(this.listContainerId, this.table.renderHeader());
        this.list.loadMore();
    }

    /**
     * @public
     */
    setSortableColumnClass(classNames) {
        this.table.getColumns().forEach(col => {
            if (col.sortable) col.setHeaderClass(classNames);
        });
    }

    /**
     * @private
     */
    _onTableReorder() {
        this.list.reset();

        Dom.setContents(this.listContainerId, this.table.renderHeader());

        this.list.loadMore();
    }

    /**
     * @public
     */
    getOrder() {
        return Table.getOrder(this.tableId);
    }
}