import('/src/JSUtils/components/ModalComponent/driver.js');

/**
 * @class
 */
class CookieConsentModalComponent extends ModalComponent {

    /**
     * @constructor
     */
    constructor() {
        super();
    
        this._setInnerTemplate(CookieConsentModalComponent.ID.TEMPLATE.THIS, {});

        this._onConsent = [];
        this._onDecline = [];
    }

    /**
     * @public
     */
    onConsent(callback) {
        this._onConsent.push(callback);
    }

    /**
     * @public
     */
    onDecline(callback) {
        this._onDecline.push(callback);
    }

    /**
     * @public
     */
    onAcceptButtonClicked() {
        this._onConsent.forEach(callback => {
            callback();
        });  
    }

    /**
     * @public
     */
    onDeclineButtonClicked() {
        this._onDecline.forEach(callback => {
            callback();
        });

        this.close();
    }
}

CookieConsentModalComponent.ID = {
    TEMPLATE: {
        THIS: 'CookieConsentModalComponent_template',
    },
};
