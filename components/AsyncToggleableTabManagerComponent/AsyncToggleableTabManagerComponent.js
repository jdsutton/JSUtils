import('../../util/Dom.js');
import('../../util/LoadAsync.js');
import('../ToggleableTabManagerComponent/ToggleableTabManagerComponent.js');

/**
 * @class
 * @param {String[]} labels
 * @param {String[]} containerIds
 * @param {String[]} componentsToLoad
 */
class AsyncToggleableTabManagerComponent extends ToggleableTabManagerComponent {

    /**
     * @constructor
     */
    constructor(labels, containerIds, componentsToLoad) {
        
        super(labels, containerIds);

        for (let i=0; i < componentsToLoad.length; i++) {
            if (!componentsToLoad[i]) continue;

            const containerId = containerIds[i];
            const componentURI = componentsToLoad[i];
            const parts = componentURI.split('/');
            const component = parts[parts.length - 1].split('.')[0];

            this._tabManager.setOnShowTab(containerId, () => {
                LoadAsync.loadHTML(componentURI, document.body).then(() => {
                    const rendered = new window[component]().render(this._facility);

                    Dom.setContents(containerId, rendered);
                });
            });
        }

        this._tabManager.showTab(containerIds[0]);
    }
}

AsyncToggleableTabManagerComponent.ID = {
    TEMPLATE: {
        THIS: 'AsyncToggleableTabManagerComponent_template',
    },
};