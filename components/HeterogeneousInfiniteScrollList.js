[[InfiniteScrollList.js]]

/**
 * @class
 * Controller for incremental-loading lists with heterogeneous content.
 * @param {String} containerId - Id of the scroll container for the list.
 * @param {?String} renderId - Id of the element to render the list elements inside of.
 * @param {Function} renderFunction
 * @param {Function} loadFunction - f(offset, limit)
 */
class HeterogeneousInfiniteScrollList extends InfiniteScrollList {

    /**
     * @constructor
     */
    constructor(containerId, renderId, renderFunction, loadFunction) {
        super(containerId, renderId, renderFunction, loadFunction);

        /**
         * @private
         * @type {Function[]}
         */
        this._auxRenderFunctions = [];

        /**
         * @private
         * @type {Object}
         */
        this._prevRenderedItem = null;
    }

    /**
     * @public
     * Specifies additional content to be rendered into the list.
     * @param {Function} renderFunction - f(prevItem, nextItem)
     * @return {HeterogeneousInfiniteScrollList} - this
     */
    interlaceContent(renderFunction) {
        this._auxRenderFunctions.push(renderFunction);

        return this;
    }

    /** 
     * @private
     * @override
     */
    _append(datum) {
        let rendered = [];
        this._data.push(datum);

        this._auxRenderFunctions.forEach(f => {
            const result = f(this._prevRenderedItem, datum);

            if (result) {
                rendered.push(result)
            }
        });

        rendered.push(this._render(datum));
        this._prevRenderedItem = datum;

        rendered.forEach(html => {
            this._renderElement.insertAdjacentHTML('beforeend', html);
            this._elements.push(this._renderElement.lastChild);
        });
    }
}