import('../Component.js');

/**
 * @class
 * @param {String[]} imageURIList
 * @param {Boolean} randomize - Whether to randomize the slideshow order. Default false.
 */
class SlideshowComponent extends Component {

    /**
     * @constructor
     */
    constructor(imageURIList, randomize) {
        if (randomize === undefined) randomize = false;

        const params = {
            hideLeft: true,
            hideRight: imageURIList.length < 2,
        };
        
        super(SlideshowComponent.ID.TEMPLATE.THIS, params);

        this._currentIndex = 0;
        this._imageURIList = null;
        this._randomize = randomize;
        this._onSlideChanged = [];

        this.setImageURIs(imageURIList);
    }

    /**
     * @private
     */
    setImageURIs(uris) {
        this._imageURIList = uris;

        if (this._randomize && uris.length) {
            this._currentIndex = Math.floor(Math.random() * this._imageURIList.length);
        }

        const image = this._imageURIList[this._currentIndex];
        if (image) {
            this._params.defaultImage = Dom.elementToHTML(this._buildElementFromFileName(image, this._currentIndex));
        }
        this._params.currentIndex = this._currentIndex;

        if (this.isRendered()) {
            this.update();
        }

        return this;
    }

    /**
     * @private
     */
    _buildElementFromFileName(fileName, index) {
        const extension = fileName.split('.').pop().split('?')[0];
        let element;

        if (this.constructor._VIDEO_FILE_EXTENSIONS.has(extension)) {
            // Video.
            element = document.createElement('video');
            element.setAttribute('type', 'video/' + extension);
            element.setAttribute('controls', 'true');
            element.loop = true;
        } else {
            // Image.
            element = document.createElement('img');
        }

        element.setAttribute('src', fileName);
        element.setAttribute('data-index', index);

        if (index == this._currentIndex) {
            element.style.display = 'flex';
        } else {
            element.style.display = 'none';
        }

        return element;
    }

    /**
     * @private
     */
    _preCacheImages() {
        let images = '';

        for (let i=0; i < this._imageURIList.length; i++) {
            if (i === 0 && this._params.defaultImage) continue;

            const fileName = this._imageURIList[i];
            const element = this._buildElementFromFileName(fileName, i);

            images = images + Dom.elementToHTML(element);
        }

        this._params.images = images;
    }

    /**
     * @public
     * @override
     */
    render() {
        this._preCacheImages();
        this._setImage();

        return super.render();
    }

    /**
     * @public
     */
    setIndex(index) {
        this._currentIndex = index % this._imageURIList.length;

        this._setImage();

        return this;
    }

    /**
     * @public
     * @param {Function} f - f(index)
     * @return {SlideshowComponent} - this
     */
    setOnSlideChanged(f) {
        this._onSlideChanged.push(f);

        return this;
    }

    /**
     * @private
     */
    _setImage() {
        if (this._currentIndex < 0) this._currentIndex = this._imageURIList.length - 1;
        this._currentIndex = this._currentIndex % (this._imageURIList.length || 1);

        const hideLeft = this._currentIndex === 0;
        const hideRight = this._currentIndex === this._imageURIList.length - 1;
        
        const image = this._imageURIList[this._currentIndex];
        if (image) {
            this._params.defaultImage = Dom.elementToHTML(this._buildElementFromFileName(image, this._currentIndex));
        }

        try {
            const parent = Dom.getById(this.getId());
            const videos = Array.from(parent.querySelectorAll('video'));
            let leftButton = this.getElement(this.constructor.ID.LEFT_BUTTON);
            let rightButton = this.getElement(this.constructor.ID.RIGHT_BUTTON);
            let images = Array.from(parent.querySelectorAll('img'));
            images = images.concat(videos);

            images.forEach(img => {
                if (Number(img.getAttribute('data-index')) === this._currentIndex) {
                    img.style.display = 'flex';

                    if (img.tagName.toLowerCase() === 'video') {
                        img.play();
                    }

                } else {
                    img.style.display = 'none';

                    if (img.tagName.toLowerCase() === 'video') {
                        img.pause();
                    }
                }
            });

            if (hideLeft) {
                leftButton.style.display = 'none';
            } else {
                leftButton.style.display = 'initial';
            }

            if (hideRight) {
                rightButton.style.display = 'none';
            } else {
                rightButton.style.display = 'initial';
            }
        }
        // Handle image index set before render.
        catch (e) {
           console.warn(e);

           this._params.hideLeft = hideLeft;
           this._params.hideRight = hideRight
        }
    }

    /**
     * @private
     */
    _handleSlideChanged() {
        this._setImage();

        this._onSlideChanged.forEach(f => {
            f(this._currentIndex);
        });
    }

    /**
     * @public
     */
    onSlideRightButtonClicked() {
        this._currentIndex++;
        this._handleSlideChanged();
    }

    /**
     * @public
     */
    onSlideLeftButtonClicked() {
        this._currentIndex--;
        this._handleSlideChanged();
    }
}

SlideshowComponent.ID = {
    LEFT_BUTTON: '_leftButton',
    RIGHT_BUTTON: '_rightButton',
    IMG: '_img',
    TEMPLATE: {
        THIS: 'SlideshowComponent_template',
    },
};
SlideshowComponent._VIDEO_FILE_EXTENSIONS = new Set([
    'webm',
    'mp4',
    'ogg',
    'avi',
]);