import('/src/JSUtils/components/Component.js');

/**
 * @class
 */
class HTMLSlideshowComponent extends Component {

    /**
     * @constructor
     */
    constructor() {
        const params = {
            slides: '',
        };
        
        super(HTMLSlideshowComponent.ID.TEMPLATE.THIS, params);

        this._slideIndex = 0;
        this._slides = [];
        this._direction = null;
        this._allowLoop = true;
    }

    /**
     * @public
     */
    allowLoop(value) {
        this._allowLoop = value;

        return this;
    }

    /**
     * @public
     */
    addSlide(html) {
        let component = new HTMLSlideComponent(html);

        this._slides.push(component);

        if (this._slides.length > 1) {
            component.setClassNames(this.constructor._CLASS.HIDDEN);
        }

        this._params.slides = this._params.slides + component.render();

        return this;
    }

    /**
     * @private
     */
    _updateClassNames() {
        const transitions = this.constructor._TRANSITION_CLASS_BY_DIRECTION[this._direction];
        const transitionsInverse = this.constructor._TRANSITION_CLASS_BY_DIRECTION
            [this._direction * -1];

        for (let i = 0; i < this._slides.length; i++) {
            const slide = this._slides[i].getId();

            if (i === this._slideIndex) {
                Dom.addClass(slide, transitions.IN);

                Dom.removeClass(slide, transitions.OUT);
                Dom.removeClass(slide, transitionsInverse.OUT);
                Dom.removeClass(slide, this.constructor._CLASS.HIDDEN);
            } else if (i === this._getRelativeIndex(this._direction * -1)) {
                Dom.addClass(slide, transitions.OUT);

                Dom.removeClass(slide, transitions.IN);
                Dom.removeClass(slide, transitionsInverse.IN);
                Dom.removeClass(slide, this.constructor._CLASS.HIDDEN);
            } else {
                Dom.addClass(slide, this.constructor._CLASS.HIDDEN);

                Dom.removeClass(slide, transitions.IN);
                Dom.removeClass(slide, transitionsInverse.IN);
                Dom.removeClass(slide, transitions.OUT);
                Dom.removeClass(slide, transitionsInverse.OUT);
            }
        }
    }

    /**
     * @private
     */
    _getRelativeIndex(off) {
        let result = (this._slideIndex + off);

        if (this._allowLoop) {
            result = (result + this._slides.length) % this._slides.length;
        } else {
            result = Math.min(result, this._slides.length - 1);
            result = Math.max(result, 0);
        }

        return result;
    }

    /**
     * @private
     */
    _slideInDirection(direction) {
        this._direction = direction;
        const newIndex = this._getRelativeIndex(this._direction);

        if (!this._allowLoop && this._slideIndex === newIndex) {
            // Reached end of slides.
            return;
        }

        this._slideIndex = newIndex;
        this._updateClassNames();
    }

    /**
     * @public
     */
    onNextButtonClicked() {
        this._slideInDirection(this.constructor._DIRECTION.FORWARD);
    }

    /**
     * @public
     */
    onPreviousButtonClicked() {
        this._slideInDirection(this.constructor._DIRECTION.BACKWARD);
    }
}

HTMLSlideshowComponent._CLASS = {
    HIDDEN: 'Hidden',
    SLIDE: 'HTMLSlideComponent',
};


HTMLSlideshowComponent._DIRECTION = {
    FORWARD: 1,
    BACKWARD: -1,
};

HTMLSlideshowComponent._TRANSITION_CLASS_BY_DIRECTION = {
    [HTMLSlideshowComponent._DIRECTION.FORWARD]: {
        IN: 'SlideInFromRight',
        OUT: 'SlideOutToLeft',
    },
    [HTMLSlideshowComponent._DIRECTION.BACKWARD]: {
        IN: 'SlideInFromLeft',
        OUT: 'SlideOutToRight',
    },
};

HTMLSlideshowComponent.ID = {
    TEMPLATE: {
        THIS: 'HTMLSlideshowComponent_template',
    },
};
