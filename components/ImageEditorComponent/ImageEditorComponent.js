import('../../storage/S3.js');
import('../Component.js');
import('../../util/Select.js'); 
import('../../io/Mouse.js');
import('../../util/Page.js');

/**
 * @class
 * @param {{href, name}[]} imageList
 * @param {ImageEditorBackendInterface} backendInterface
 * @param {?Number} cropWidth
 * @param {?Number} cropHeight
 */
class ImageEditorComponent extends Component {

    /**
     * @constructor
     */
    constructor(imageList, backendInterface, cropWidth, cropHeight) {
        super(ImageEditorComponent.TEMPLATE_ID, {});

        this._imageList = imageList;

        /**
         * @private
         * @type {ImageEditorBackendInterface}
         */
        this._backendInterface = backendInterface
        this._imageNamesByURI = {};
        this._s3KeysByURI = {};
        this._cropWidth = cropWidth;
        this._cropHeight = cropHeight;
        this._modalComponent = null;
        this._imageRef = null;
        this._constrainSize = false;
        this._imageAlign = null;

        /**
         * @private
         * @type {?S3FileManager}
         */
        this._s3FileManager = null;

        /**
         * @private
         * @type {{href, name}[]}
         */
        this._imageList = [];

        /**
         * @private
         */
        this._onImageUpdated = [];

        this._initParams();
        this._handleImageList(imageList, false, true);

        Page.addResizeEvent(() => {
            this._onResize();
        });
    }

    _initParams() {
        let imageWidth, imageHeight;

        if (this._cropWidth) {
            imageWidth = this._cropWidth + 'px';
        } else {
            imageWidth = '100%';
        }

        if (this._cropHeight) {
            imageHeight = this._cropHeight + 'px';
        } else {
            imageHeight = '100%';
        }

        let imageSrc = '/images/image.svg';
        if (this._imageList.length) imageSrc = this._imageList[0].href;

        this._params = Object.assign(this._params, {
            imageSrc,
            imageWidth,
            imageHeight,
            constrainSize: this._constrainSize,
            imageAlign: this._imageAlign,
            notEditable: !this._canModifyImage(),
            message: this._getMessage(),
        });
    }

    /**
     * @private
     */
    _getMessage() {
        if (!this._canModifyImage()) {
            let width;

            try {
                width = this._getPreviewImageElement().parentNode.clientWidth;
            } catch (e) {
                width = '?';
            }

            return `This is how your image will appear on a display ${width} pixels wide.&nbsp;`;
        }

        return null;
    }

    /**
     * @private
     */
    _onResize() {
        const messageId = this.getId() + this.constructor.ID.MESSAGE;

        Dom.setContents(messageId, this._getMessage());
    }

    /**
     * Only accept images of the correct size.
     * Disables scaling.
     * @public
     * @return {ImageEditorComponent} - this
     */
    constrainImageSize() {
        this._constrainSize = true;

        this._initParams();

        return this;
    }

    /**
     * Set image alignment.
     * Only valid with constrainImageSize().
     * @param {'left'|'right'|'center'} align
     * @return {ImageEditorComponent} - this;
     */
    setImageAlignment(align) {
        align = align.charAt(0).toUpperCase() + align.slice(1); 
        this._imageAlign = align;

        this._initParams();
        
        return this;
    }

    /**
     * @private
     */
    _handleImageList(imageList, refreshFileList, isFirstLoad) {
        imageList = imageList.filter(file => {
            // Filter out thumbnails.
            let fileName = file.name.split('.');
            const extension = fileName.pop();
            fileName = fileName.join('.');

            if (extension === 'svg') return false;
            if (fileName.endsWith('_thumbnail')) return false;

            return true;
        });

        this._imageList = imageList;

        imageList.forEach(file => {
            const parts = file.name.split('/');
            this._imageNamesByURI[file.href] = parts[parts.length - 1];
            this._s3KeysByURI[file.href] = file.name;
        });

        if (!imageList.length && isFirstLoad) {
            this._params.imageSrc = ImageEditorComponent.IMAGE.SPINNER;
        }

        // Refresh file list if needed.
        if (refreshFileList) {
            this.update();

            if (!imageList.length && !isFirstLoad) {
                this._showUploadMessage('No files uploaded.');
            }
        }
    }

    /**
     * @public
     * @param {S3FileManager} s3FileManager
     * @return {ImageEditorComponent} this
     */
    allowUploadFiles(s3FileManager) {
        this._s3FileManager = s3FileManager;
        this._params.showUploadFileUI = true;

        s3FileManager.getFileList().then(files => {
            this._handleImageList(files, true /* refreshFileList */)
        });

        return this;
    }

    /**
     * Called when the image is saved in the editor.
     * See setOnNewImageLoaded().
     * @public
     * @param {Function} f - f(imageURI)
     * @return {ImageEditorComponent} this
     */
    setOnImageUpdated(f) {
        this._onImageUpdated.push(f);

        return this;
    }

    /**
     * Called when an updated image is fully loaded and ready to be rendered.
     * @public
     * @param {Function} f - f(s3Object)
     * @return {ImageEditorComponent} this
     */
    setOnNewImageLoaded(f) {
        this.setOnImageUpdated(uri => {
            let key = decodeURIComponent(this._getS3Key(uri));
            const parts = key.split('.');
            key = parts.slice(0, parts.length - 1) + '_thumbnail.' + parts[parts.length - 1];

            console.debug('Image updated. Waiting for update to key: ' + key);

            this._s3FileManager.setOnKeyUpdated(key, f);
        });

        return this;
    }

    /**
     * @return {String} - The rendered HTML.
     */
    render() {
        this._params.imageSelector = (new Select('image', this._imageNamesByURI))
            .setOnChange(() => {
                console.debug('Selected image changed.')
                Component.get(_id).onImageChanged();
            }, {_id: this.getId()})
            .render();

        this._modalComponent = new ModalComponent().setContent(super.render());

        return this._modalComponent.render();
    }

    /**
     * @public
     * @override
     */
    update() {
        this._initParams();
        Dom.replaceWith(this._modalComponent.getId(), this.render());
    }

    /**
     * @public
     */
    close() {
        Dom.removeElement(this._modalComponent.getId());
    }

    _getImageLabel() {
        let data = Dom.getDataFromForm(this._id);
        
        return this._s3KeysByURI[data.image];
    }

    /**
     * Returns all form data from this component.
     */
    getData() {
        let data = Dom.getDataFromForm(this._id);
        data.s3Key = this._getImageLabel();

        return data;
    }

    /**
     * Called when the image scale is changed.
     */
    onScaleChanged() {
        if (!this._canModifyImage()) return;

        const scale = Dom.getById(this.constructor.ID.IMAGE_SCALE_INPUT).value;
        const previewImageElement = this._getPreviewImageElement();
        let tmpImg = new Image();
        tmpImg.src = previewImageElement.src;

        // Override style (image will be cropped).
        previewImageElement.style.maxWidth = '10000px';
        previewImageElement.style.maxHeight = '10000px';

        previewImageElement.width = tmpImg.width * scale;
        previewImageElement.height = tmpImg.height * scale;
    }

    /**
     * @return {Element}
     */
    _getPreviewImageElement() {
        return Dom.getById(this._id).getElementsByTagName('img')[0];
    }

    /**
     * Called when the selected image is changed.
     */
    onImageChanged() {
        const image = this.getData().image;
        let slider = this._getScaleSliderElement();

        this._getPreviewImageElement().src = image;


        slider.value = 1;
    }

    _getScaleSliderElement() {
        return Dom.getById(this.constructor.ID.IMAGE_SCALE_INPUT);
    }

    /**
     * Called when the mouse is moved within the image container.
     * @param {Event}
     */
    onMouseMoved(event) {
        if (!this._canModifyImage()) return;

        let imageElement = this._getPreviewImageElement();

        if (!Mouse.isDown()) {
            this._initialMousePosition = Mouse.getPositionOnPage(event);
            this._initialImagePosition = {
                x: imageElement.offsetLeft,
                y: imageElement.offsetTop
            }
            return;
        }
        if (!this._initialMousePosition) return;

        // Mouse is dragging across image.
        const mousePosition = Mouse.getPositionOnPage(event);

        imageElement.style.left = this._initialImagePosition.x
            + Math.round(mousePosition.x - this._initialMousePosition.x) + 'px';
        imageElement.style.top = this._initialImagePosition.y
            + Math.round(mousePosition.y - this._initialMousePosition.y) + 'px';
    }

    _canModifyImage() {
        if (this._constrainSize || this._imageAlign) return false;

        let label;

        try {
            label = this._getImageLabel();
        } catch (e) {
            console.debug(e);

            return false;
        }

        return (label !== 'Default');
    }

    /**
     * @public
     */
    getOffset() {
        const imageElement = this._getPreviewImageElement();

        if (this._imageAlign === 'Center') {
            const size = this.getSize();
            const img = this._getPreviewImageElement();

            return {
                x: Math.floor(-img.naturalWidth / 2 + size.width / 2),
                y: Math.floor(-img.naturalHeight / 2 + size.height / 2),
            };
        }

        if (this._constrainSize) {
            return {
                x: 0,
                y: 0,
            };
        }

        return {
            x: imageElement.offsetLeft,
            y: imageElement.offsetTop,
        };
    }

    /**
     * @public
     */
    getScale() {
        return Number(this._getScaleSliderElement().value);
    }

    /**
     * @public
     */
    getSize() {
        const image = this._getPreviewImageElement();

        return {
            width: this._cropWidth || image.naturalWidth,
            height: this._cropHeight || image.naturalHeight
        };
    }

    /**
     * @public
     */
    getImage() {
        return this._imageRef;
    }

    /**
     * @public
     */
    onSaveButtonClicked() {
        const data = this.getData();
        this._imageRef = data.image;

        // Generate thumbnail for preview card.
        const offset = this.getOffset();
        const scale = this.getScale();
        const size = this.getSize();

        this._backendInterface.translateCropAndScaleImage(data.s3Key, offset.x, offset.y, scale,
            size.width, size.height);

        const driver = Driver.getViewDriver();
        if (driver.setImage) {
            driver.setImage(this._imageRef);
        } else {
            // console.log('Warning: View driver must have a .setImage(uri) method.');
        }

        this._onImageUpdated.forEach(f => {
            f(this._imageRef);
        });

        this.close();
    }

    /**
     * @public
     */
    onUploadFileButtonClicked() {
        const fileInput = Dom.getById(this.getId() + this.constructor.ID.FILE_INPUT);
    
        fileInput.click();
    }

    /**
     * @private
     * @param {String} message
     */
    _showUploadMessage(message) {
        const container = Dom.getById(this.getId()
            + this.constructor.ID.FILE_UPLOAD_MESSAGE_CONTAINER);

        Dom.setContents(container, message);
    }

    /**
     * @private
     */
    _getImageFromFile(f) {
        let reader = new FileReader();

        let result = new Promise((resolve, reject) => {
            reader.onload = () => {
                let img = new Image();

                img.onload = () => {
                    resolve(img);
                }

                img.src = reader.result;
            }
        });

        reader.readAsDataURL(f);

        return result;
    }

    /**
     * @private
     */
    _getS3Key(uri) {
        return S3FileManager.basename(uri);
    }

    /**
     * @public
     * @param {Element} element
     */
    onFileSelected(element) {
        const file = element.files[0];
        const key = this._getS3Key(file.name);

        if (this._s3FileManager.hasFile(key)) {
            this._showUploadMessage('Error: File already exists.');

            return;
        }

        this._getImageFromFile(file).then(img => {
            // Check image dimensions if needed.
            if (this._constrainSize
                && !(img.width === this._cropWidth && img.height === this._cropHeight)) {
                
                this._showUploadMessage('Error: Image dimensions incorrect.');

                return;
            }

            this._showUploadMessage('Uploading...');

            this._s3FileManager.upload(element, key).then(files => {
                this._handleImageList(files, true /* refreshFileList */)
            });
        });
    } 
}

// For image translation.
ImageEditorComponent._initialImagePosition = null;
ImageEditorComponent._initialMousePosition = null;

ImageEditorComponent.CLASS = {
    NO_TRANSITION: 'NoTransition'
};

ImageEditorComponent.ID = {
    FILE_INPUT: '_fileInput',
    FILE_UPLOAD_MESSAGE_CONTAINER: '_fileUploadMessageContainer',
    IMAGE_SCALE_INPUT: 'imageScaleInput',
    MESSAGE: '_message',
    TEMPLATE: {
        MODAL: 'modalTemplate',
        PREVIEW_CARD: 'salesCardPreviewTemplate'
    }
};

ImageEditorComponent.IMAGE = {
    SPINNER: 'https://s3.amazonaws.com/eek-webapp-static/eek_loading_spinner.gif',
};

ImageEditorComponent.TEMPLATE_ID = 'ImageEditorTemplate';
