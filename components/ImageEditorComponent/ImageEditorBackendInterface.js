/**
 * @class
 * Allows transformations to be performed on uploaded and validated image files.
 */
class ImageEditorBackendInterface {

    /**
     * Transforms an image and saves it as a copy with "_thumbnail" appended to the key.
     * @abstract
     * @param {String} key - The key of the image to transform.
     * @param {Number} x - The X offset to translate the image by.
     * @param {Number} y - The Y offset to translate the image by.
     * @param {Number} scale - The ratio to scale the image by.
     * @param {Number} width - The width to crop the image down to.
     * @param {Number} height - The height to crop the image down to.
     * @return {Promise}
     */
    static translateCropAndScaleImage(key, x, y, scale, width, height, bucketName) {}
}