import('../Component.js');

/**
 * @class
 * @param {String} imageURI
 * @param {String} textOverlay
 * @param {Number} heightPx - default 300.
 */
class BannerComponent extends Component {

    /**
     * @constructor
     */
    constructor(imageURI, textOverlay, heightPx) {
        super(BannerComponent.ID.TEMPLATE.THIS, {
            imageURI,
            textOverlay,
            height: (heightPx || 300) + 'px',
        });
    }
}


BannerComponent.CLASS = {

};

BannerComponent.ID = {
    TEMPLATE: {
        THIS: 'BannerTemplate'
    }
};
