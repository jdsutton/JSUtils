import('/src/JSUtils/util/Currency.js');
import('/src/JSUtils/util/FormValidation.js');
import('/src/JSUtils/util/Page.js');

let stripe;
Page.addLoadEvent(() => {
    if (!'[["stripePublishableKey"]]'.length) {
        console.warn('Config key missing; stripePublishableKey');
    }

    stripe = Stripe('[["stripePublishableKey"]]');
});

/**
 * @typedef Charges
 * @property {ChargeItem[]} items
 * @property {?Number} transactionFeeUSCents
 * @property {Number} totalUSCents
 */

/**
 * @class
 * A modal component for checkout with Stripe.
 * Uses environment variables: stripePublishableKey, supportEmail
 * @param {?Object[]} products
 * @param {Function} getChargesF - Returns a Promise<Charges>
 * @param {?Function} purchaseF
 * @param {String} purchasedProductsURI
 * {@link https://stripe.com/docs/stripe-js/reference}
 * {@link https://stripe.com/docs/stripe-js/elements/quickstart}
 * {@link https://stripe.com/docs/testing#cards}
 */
class StripeCheckoutFormModalComponent extends ModalComponent {

    /**
     * @constructor
     */
    constructor(products, getChargesF, purchaseF, purchasedProductsURI) {
        super();
    
        /**
         * @private
         * @type {Product[]}
         */
        this._products = products || [];
        this._getChargesF = getChargesF;
        this._purchaseF = purchaseF || (() => Promise.resolve(null));
        this._purchasedProductsURI = purchasedProductsURI;
        this._card = null;
        this._requireEmail = false;
        this._email = null;
        this._closeable = true;

        this.setOnRender(() => {
            this._initStripe();
        });
    }

    /**
     * @public
     */
    requireEmail() {
        this._requireEmail = true;

        return this;
    }

    /**
     * @public
     * @override
     */
    render() {
        this._getChargesF(this._products).then(charges => {
            let params = Object.assign({
                charges: charges ? this._renderCharges(charges.items) : null,
                total: charges ? Currency.formatUSD(charges.totalUSCents) : null,
                requireEmail: this._requireEmail,
            }, this._params);

            if (charges && charges.transactionFeeUSCents) {
                params.transactionFee = Currency.formatUSD(charges.transactionFeeUSCents);
            }

            const content = JSRender.render(this.constructor.ID.TEMPLATE.THIS, params);

            this.setContent(content);

            const rendered = super.render();

            Dom.appendTo(document.body, rendered);
        });
    }

    /**
     * @private
     */
    _renderCharges(items) {
        let result = '';

        items.forEach(item => {
            const price = Currency.formatUSD(item.priceUSCents);
            result = result + `<div>${item.name}: ${price}</div>`;
        });

        return result;
    }

    /**
     * @private
     */
    _initStripe() {
        let elements = stripe.elements();
        const style = {
          base: {
            // Add your base input styles here. For example:
            fontSize: '16px',
            color: "#32325d",
          }
        };

        this._card = elements.create('card', {style});
        this._card.mount('#' + this.getId() + this.constructor.ID.CARD);
    }

    /**
     * @private
     */
    _getValidators() {
        let result = {};

        if (this._requireEmail) {
            result['email'] = [
                new FormValidation.EmailValidator(),
            ];
        }

        return result;
    }

    /**
     * @public
     */
    onSubmitPaymentClicked() {
        let paymentButton = Dom.getById(this.getId() + this.constructor.ID.PAYMENT_BUTTON);
        paymentButton.disabled = true;

        // Validation.
        const validators = this._getValidators();
        const data = Dom.getDataFromForm(this.getId(), false, validators);

        if (this._requireEmail) {
            if (!data) {
                paymentButton.disabled = false;

                return;
            }

            this._email = data.email;

            console.assert(this._email.length);
        }

        // Submit payment.
        this._closeable = false;
        Dom.setContents(paymentButton, 'Processing, please wait...');

        stripe.createToken(this._card).then(result => {
            if (result.error) {
                // Inform the customer that there was an error.
                this._showErrorMessage(result.error.message);
                Dom.setContents(paymentButton, 'Error!');

                // Let them try again after a timeout.
                setTimeout(() => {
                    Dom.setContents(paymentButton, 'Submit Payment');
                    paymentButton.disabled = false;
                    this._closeable = true;
                }, StripeCheckoutFormModalComponent._ERROR_TIMEOUT_MS);
            } else {
                this._submitPayment(result.token);
            }
        });
    }

    /**
     * @private
     */
    _showErrorMessage(message) {
        const errorId = this.getId() + this.constructor.ID.ERRORS;
        const errorElement = document.getElementById(errorId);

        errorElement.textContent = message;
    }

    /**
     * @private
     */
    _submitPayment(token) {
        const handleErr = err => {
            console.error(err);

            this._showErrorMessage(this.constructor._ERROR_MESSAGE);
        };

        try {
            this._purchaseF(this._products, token, this._email).then(result => {
                window.location = this._purchasedProductsURI;
            }).catch(handleErr);
        } catch (e) {
            handleErr(e);
        }

        this._closeable = true;
    }

    /**
     * @public
     * @override
     */
    close() {
        if (!this._closeable) return;

        super.close();
    }
}

StripeCheckoutFormModalComponent.ID = {
    CARD: '_card',
    ERRORS: '_errorsContainer',
    PAYMENT_BUTTON: '_paymentButton',
    TEMPLATE: {
        THIS: 'StripeCheckoutFormModalComponent_template',
    },
};

StripeCheckoutFormModalComponent._ERROR_MESSAGE = 'Something went wrong.'
    + ' Please contact us at [["supportEmail"]] for support.';
StripeCheckoutFormModalComponent._ERROR_TIMEOUT_MS = 3000;
