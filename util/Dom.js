import('generic.js');
import('FormValidation.js');

/**
 * Contains functions for manipulating and querying the DOM.
 * @public
 */
class Dom {
    /**
     * Returns an element with the given id.
     * @static
     */
    static getById(id) {
        if (id instanceof Element) return id;

        const result = document.getElementById(id);

        if (!result) {
            throw 'Error: no element found with id: '+ id;
        }

        return result;
    }

    /**
     * Removes an element from the DOM.
     * @static
     * @param {string} id
     */
    static removeElementById(id) {
        const element = document.getElementById(id);
        element.parentNode.removeChild(element);
    }

    /**
     * Removes an element from the DOM.
     * @static
     * @param {Element|String} element - The element or ID of the element to remove.
     */
    static removeElement(element) {
        if (typeof(element) === 'string') {
            element = Dom.getById(element);
        }

        if (!element) return;

        element.parentNode.removeChild(element);
    }

    /**
     * Appends html to an element.
     * @param {Element|String} element - Element or ID of element to append into.
     * @param {Element|String} html
     */
    static appendTo(element, html) {
        if (typeof(element) === 'string') {
            element = Dom.getById(element);
        }

        if (html instanceof Element) {
           element.appendChild(html)
        } else {
            element.insertAdjacentHTML('beforeend', html);
        }
    }

    /**
     * @public
     */
    static prependTo(element, html) {
        if (typeof(element) === 'string') {
            element = Dom.getById(element);
        }
        if (html instanceof Element) {
            html = Dom.elementToHTML(html);
        }
        element.insertAdjacentHTML('afterbegin', html);
    }

    /**
     * @public
     */
    static replaceWith(element, html) {
        if (typeof(element) === 'string') {
            element = Dom.getById(element);
        }
        if (typeof(html) === 'string') {
            html = Dom.htmlToElement(html.trim());
        }
        element.parentNode.replaceChild(html, element);
    }

    /**
     * Inserts a new element after an existing one.
     * @param {Element} referenceElement
     * @param {Element} newElement
     */
    static insertAfter(referenceElement, newElement) {
        referenceElement.parentNode.insertBefore(newElement,
            referenceElement.nextSibling);
    }

    /**
     * Returns a list of child elements by name.
     * @param {Element|string} element - The parent element.
     * @param {String} name
     * @return {Element[]}
     */
    static getChildrenByName(element, name) {
        if (typeof(element) === 'string') element = Dom.getById(element);

        return element.querySelectorAll('[name=' + name + ']');
    }

    /**
     * Returns a list of Elements with the given class.
     * @param {String} className
     * @return {Element[]}
     */
    static getByClass(className) {
        return Array.from(document.getElementsByClassName(className));
    }

    /**
     * Returns a list of Elements with the given tag.
     * @param {String} tag
     * @return {Element[]}
     */
    static getByTag(tag) {
        return Array.from(document.getElementsByTagName(tag));
    }
 
    /**
     * Returns the first child of an element with the given tag.
     * @static
     * @param {Element|String} element
     * @param {String} tag
     * @returns {element}
     */
    static getFirstChildByTag(element, tag) {
        if (typeof(element) === 'string') {
            element = Dom.getById(element);
        }
        tag = tag.toUpperCase();
        for (var i = 0; i < element.children.length; i++) {
            if (element.children[i].tagName === tag) {
                return element.children[i];
            }
        }

        return null;
    }

    /**
     * @public
     */
    static getChildrenByTag(element, tag) {
        if (typeof(element) === 'string') {
            element = Dom.getById(element);
        }

        return Array.from(element.getElementsByTagName(tag));
    }

    /**
     * @public
     */
    static getChildren(element) {
        if (typeof(element) === 'string') {
            element = Dom.getById(element);
        }
        
        return Array.from(element.children);
    }

    /**
     * Returns the first anscestor of an element with the given class.
     * @param {Element} element
     * @param {String} cls
     * @returns {Element}
     */
    static anscestorWithClass(element, cls) {
        while (element.parentElement) {
            if (element.parentElement.className.split(' ').indexOf(cls) != -1) {
                return element.parentElement;
            }
            element = element.parentElement;
        }

        return null;
    }

    /**
     * Adds a CSS class to the given element.
     * @param {Element|String} element
     * @param {String} cls
     */
    static addClass(element, cls) {
        if (typeof(element) === 'string') {
            element = Dom.getById(element);
        }
        if (element.classList) {
            element.classList.add(cls);
        }
        else {
            element.className += ' ' + cls;
        }
    }

    /**
     * Removes CSS class from the given element.
     * @param {Element|String} element
     * @param {String} cls
     */
    static removeClass(element, cls) {
        if (typeof(element) === 'string') {
            element = Dom.getById(element);
        }
        if (element.classList) {
            element.classList.remove(cls);
        }
        else {
            const pattern = new RegExp('/(?:^|\s)' + cls + '(?!\S)/g');
            element.className = element.className.replace(pattern, '');
        }
    }

    /**
     * @param {Element|String} element
     * @param {String} cls
     */
    static toggleClass(element, cls) {
        if (typeof(element) === 'string') element = Dom.getById(element);
        
        if (element.className.indexOf(cls) > -1) {
            Dom.removeClass(element, cls);
        } else {
            Dom.addClass(element, cls);
        }
    }

    /**
     * @param {Element|String} element
     * @param {String} cls
     */
    static hasClass(element, cls) {
        if (typeof(element) === 'string') element = Dom.getById(element);
        
        return element.className.split(' ').indexOf(cls) > -1;
    }

    static _getInputs(form) {
        if (typeof(form) === 'string') form = Dom.getById(form);

        let inputs = Array.from(form.getElementsByTagName('input'));
        const textareas = Array.from(form.getElementsByTagName('textarea'));
        inputs = inputs.concat(textareas);

        return inputs;
    }

    /**
     * @public
     * Sets form data by input name.
     */
    static setFormData(form, data) {
        let inputs = this._getInputs(form);

        inputs.forEach(input => {
            const value = data[input.name];

            if (value !== undefined) {
                input.value = value;
            }
        });
    }

    /**
     * Returns data from a form's inputs.
     * @param {String|Element} form
     * @param {?Boolean} clearForm - Whether to remove all values from the form. Default false.
     * @param {?Object<string, FormValidation.Validator[]>}
     * @return {?Object<string, string>} - The values of the form by input name. Null if validation fails.
     */
    static getDataFromForm(form, clearForm, validators, removeErrors) {
        if (typeof(form) === 'string') form = Dom.getById(form);

        clearForm = clearForm || false;

        let inputs = this._getInputs(form);
        let result = {};
        let namesSeen = new Set();

        for (let input of inputs) {
            if (!input.name) continue;

            let value;
            
            if (input.type === 'checkbox') {
                if (input.hasAttribute('value')) {
                    if (input.checked)
                        value = input.getAttribute('value');
                } else {
                    value = input.checked;
                }

                if (clearForm) input.checked = false;
            } else {
                value = input.value;
                
                if (clearForm) input.value = '';
            }

            if (namesSeen.has(input.name)) {
                const currentValue = result[input.name];

                // Handle repeated values for same name.
                if (!Array.isArray(currentValue)) {
                    result[input.name] = [];

                    if (currentValue !== undefined)
                        result[input.name].push(currentValue);
                }
            }

            if (value !== undefined) {
                if (!namesSeen.has(input.name)) {
                    // First time.
                    result[input.name] = value;
                } else {
                    result[input.name].push(value);
                }
            }

            namesSeen.add(input.name);
        }

        const selects = Array.from(form.getElementsByTagName('select'));
        selects.forEach(select => {
            if (select.selectedIndex > -1) {
                result[select.name] = select.options[select.selectedIndex].value;
            }
        });

        if (validators) {
            const errorsFound = FormValidation.validate(form.id, result, validators, removeErrors);

            if (errorsFound) return null;
        }

        return result;
    }

    /**
     * Sets the inner HTML on an element.
     * @param {Element|String} element - The element or id of the element to change.
     * @param {Element|String} html - The HTML to insert.
     */
    static setContents(element, html) {
        if (typeof(element) === 'string') {
            element = Dom.getById(element);
        }
        if (html instanceof Element) {
            element.innerHTML = '';
            element.appendChild(html);
        } else {
            element.innerHTML = html;
        }
    }

    /** 
     * @param {Element} element
     * @return {String}
     */
    static elementToHTML(element) {
        let wrap = document.createElement('div');
        wrap.appendChild(element.cloneNode(true));

        return wrap.innerHTML;
    }

    /**
     * @public
     */
    static htmlToElement(html) {
        let div = document.createElement('div');
        div.innerHTML = html.trim();

        return div.firstChild;
    }

    /**
     * @public
     */
    static setOnClick(element, f, values) {
        values = values || {};
        if (typeof(element) === 'string') {
            element = Dom.getById(element);
        }

        element.setAttribute('onclick', fToString(f, values));
    }
}