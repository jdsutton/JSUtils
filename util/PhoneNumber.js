/**
 * @class
 */
 class PhoneNumber {

    /**
     * @public
     * https://stackoverflow.com/questions/8358084/regular-expression-to-reformat-a-us-phone-number-in-javascript
     */
    static format(number) {
        if (!number) return null;

        number = String(number);

        // Remove non-numerals.
        number = number.replace(/\D/g, '');

        const digits = number.match(/^(\d{3})(\d{3})(\d{4})$/);

        if (!digits) return null;

        return `(${digits[1]}) ${digits[2]}-${digits[3]}`;
    }
 }