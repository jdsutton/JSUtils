const suite = describe;
const spec = it;

function assertEqual(a, b) {
    if (a !== b) {
        throw new Error(`Assertion Error: actual: ${a} !== expected: ${b}.`);
    }
}

function assertDeepEqual(a, b) {
    const actual = JSON.stringify(a);
    const expected = JSON.stringify(b);

    if (actual !== expected) {
        throw new Error(`Assertion Error: actual: ${actual} !== expected: ${expected}.`);
    }
}

function assertAlmostEqual(a, b, epsilon) {
    epsilon = epsilon || 0.00001;

    if (Math.abs(a - b) > epsilon){
        throw new Error(`Assertion Error: actual: ${a} and expected: ${b} not within ${epsilon}.`);
    }
}

function assertNotEqual(a, b) {
    if (a === b) {
        throw new Error(`Assertion Error: actual: ${a} === expected: ${b}.`);
    }
}

function assertTrue(value) {
    if (!value) {
        throw new Error(`Assertion Error: ${value} is not true.`);
    }
}
