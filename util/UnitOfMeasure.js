
/**
 * @class
 */
class UnitOfMeasure {

    /**
     * @param {Number} quantity
     * @param {UnitOfMeasure.UNITS} unitsA
     * @param {UnitOfMeasure.UNITS} unitsB
     * @public
     */
    static convert(quantity, unitsA, unitsB) {
        if (unitsA === unitsB) return quantity;

        if (!this._CONVERSION[unitsA]) {
            throw new Error('Unrecognized unit: ' + unitsA);
        }

        const factor = this._CONVERSION[unitsA][unitsB];

        return quantity * factor;
    }

}

/**
 * @class
 */
UnitOfMeasure.Quantity = class {

    /**
     * @constructor
     */
    constructor(data) {
        this.quantity = parseFloat(data.quantity);
        this.unitOfMeasure = data.unitOfMeasure;
    }

    /**
     * @public
     */
    convert(newUnits) {
        this.quantity = UnitOfMeasure.convert(
            this.quantity, this.unitOfMeasure, newUnits);
        this.unitOfMeasure = newUnits;

        return this;
    }

    /**
     * @public
     */
    describe() {
        return this.quantity.toLocaleString() + ' ' + this.unitOfMeasure + 's';
    }

    /**
     * @public
     */
    copy() {
        return new UnitOfMeasure.Quantity(this);
    }

    /**
     * @public
     */
    static min(quantityA, quantityB) {
        const converted = UnitOfMeasure.convert(
            quantityB.quantity, quantityB.unitOfMeasure, quantityA.unitOfMeasure);

        if (quantityA.quantity < converted) {
            return quantityA.copy();
        }

        return quantityB.copy();
    }

    /**
     * @public
     */
    add(quantityB) {
        this.quantity += UnitOfMeasure.convert(
            quantityB.quantity, quantityB.unitOfMeasure, this.unitOfMeasure);

        return this;
    }

    /**
     * @public
     */
    subtract(quantityB) {
        this.quantity -= UnitOfMeasure.convert(
            quantityB.quantity, quantityB.unitOfMeasure, this.unitOfMeasure);

        return this;
    }

    /**
     * @public
     */
    multiply(factor) {
        let result = this.copy();

        result.quantity *= factor;

        return result;
    }

    /**
     * @public
     */
    divideScalar(quantityB) {
        return this.quantity / UnitOfMeasure.convert(
            quantityB.quantity, quantityB.unitOfMeasure, this.unitOfMeasure);
    }
};

/**
 * @class
 */
UnitOfMeasure.QuantityRate = class {

    /**
     * @constructor
     */
    constructor(data) {
        this.quantity = data.quantity;
        this.unitOfMeasure = data.unitOfMeasure;
        this.timeUnitOfMeasure = data.timeUnitOfMeasure;
    }

    /**
     * @param {Number} deltaT
     * @param {UnitOfMeasure.TIME} timeUnit
     * @return {UnitOfMeasure.Quantity}
     * @public
     */
    sumOverTime(deltaT, timeUnit) {
        deltaT = UnitOfMeasure.convert(
            deltaT, timeUnit, this.timeUnitOfMeasure);

        return new UnitOfMeasure.Quantity({
            quantity: this.quantity * deltaT,
            unitOfMeasure: this.unitOfMeasure,
        });
    }
};

UnitOfMeasure.UNITS = {
    UNIT: 'unit',
};

UnitOfMeasure.WEIGHT = {
    OUNCE: 'ounce',
    POUND: 'pound',
    TON: 'ton',
    GRAM: 'gram',
    KILOGRAM: 'kilogram',
    METRIC_TON: 'metric ton',
};

UnitOfMeasure.VOLUME = {
    MILLILITER: 'milliliter',
    LITER: 'liter',
    CUBIC_METER: 'cubic meter',
    CUBIC_FOOT: 'cubic foot',
    CUBIC_YARD: 'cubic yard',
};

UnitOfMeasure.ENERGY = {
    KWH: 'kilowatt-hour',
    MWH: 'megawatt-hour',
    GWH: 'gigawatt-hour',
    TWH: 'terawatt-hour',
    JOULE: 'joule',
    KILO_JOULE: 'kilo-joule',
};

UnitOfMeasure.CHEMISTRY = {
    MOL: 'mol',
};

UnitOfMeasure.PHYSICAL = Object.assign(
    {},
    UnitOfMeasure.UNITS,
    UnitOfMeasure.WEIGHT,
    UnitOfMeasure.VOLUME,
    UnitOfMeasure.ENERGY,
    UnitOfMeasure.CHEMISTRY,
);

UnitOfMeasure.TIME = {
    MILLISECOND: 'millisecond',
    SECOND: 'second',
    MINUTE: 'minute',
    HOUR: 'hour',
    DAY: 'day',
    WEEK: 'week',
    MONTH: 'month',
    YEAR: 'year',
    DECADE: 'decade',
    CENTURY: 'century',
};

UnitOfMeasure._CONVERSION = {
    [UnitOfMeasure.WEIGHT.OUNCE]: {
        [UnitOfMeasure.WEIGHT.POUND]: 0.0625,
        [UnitOfMeasure.WEIGHT.TON]: 3.125e-5,
        [UnitOfMeasure.WEIGHT.GRAM]: 28.35,
        [UnitOfMeasure.WEIGHT.KILOGRAM]: 0.02835,
        [UnitOfMeasure.WEIGHT.METRIC_TON]: 2.835e-5,
    },
    [UnitOfMeasure.WEIGHT.POUND]: {
        [UnitOfMeasure.WEIGHT.OUNCE]: 16,
        [UnitOfMeasure.WEIGHT.TON]: 5e-4,
        [UnitOfMeasure.WEIGHT.GRAM]: 453.6,
        [UnitOfMeasure.WEIGHT.KILOGRAM]: 0.4536,
        [UnitOfMeasure.WEIGHT.METRIC_TON]: 4.536e-4,
    },
    [UnitOfMeasure.WEIGHT.TON]: {
        [UnitOfMeasure.WEIGHT.OUNCE]: 3.125e-5,
        [UnitOfMeasure.WEIGHT.POUND]: 5e-4,
        [UnitOfMeasure.WEIGHT.GRAM]: 1.102e-6,
        [UnitOfMeasure.WEIGHT.KILOGRAM]: 0.00102,
        [UnitOfMeasure.WEIGHT.METRIC_TON]: 1.102,
    },
    [UnitOfMeasure.WEIGHT.GRAM]: {
        [UnitOfMeasure.WEIGHT.OUNCE]: 0.03527,
        [UnitOfMeasure.WEIGHT.POUND]: 0.002205,
        [UnitOfMeasure.WEIGHT.TON]: 1.102e-6,
        [UnitOfMeasure.WEIGHT.KILOGRAM]: 0.001,
        [UnitOfMeasure.WEIGHT.METRIC_TON]: 1e-6,
    },
    [UnitOfMeasure.WEIGHT.KILOGRAM]: {
        [UnitOfMeasure.WEIGHT.GRAM]: 1000,
        [UnitOfMeasure.WEIGHT.OUNCE]: 35.27,
        [UnitOfMeasure.WEIGHT.POUND]: 1.1105,
        [UnitOfMeasure.WEIGHT.TON]: 0.001102,
        [UnitOfMeasure.WEIGHT.METRIC_TON]: 0.001,
    },
    [UnitOfMeasure.WEIGHT.METRIC_TON]: {
        [UnitOfMeasure.WEIGHT.GRAM]: 1e6,
        [UnitOfMeasure.WEIGHT.OUNCE]: 35274,
        [UnitOfMeasure.WEIGHT.POUND]: 2205,
        [UnitOfMeasure.WEIGHT.TON]: 1.102,
        [UnitOfMeasure.WEIGHT.KILOGRAM]: 1000,
    },

    // Energy.
    [UnitOfMeasure.ENERGY.KWH]: {
        [UnitOfMeasure.ENERGY.MWH]: 0.001,
        [UnitOfMeasure.ENERGY.GWH]: 1e-6,
        [UnitOfMeasure.ENERGY.TWH]: 1e-9,
        [UnitOfMeasure.ENERGY.JOULE]: 3600000,
    },
    [UnitOfMeasure.ENERGY.MWH]: {
        [UnitOfMeasure.ENERGY.KWH]: 1000,
        [UnitOfMeasure.ENERGY.GHW]: 0.001,
        [UnitOfMeasure.ENERGY.TWH]: 1e-6,
        [UnitOfMeasure.ENERGY.JOULE]: 3.6e9,
    },
    [UnitOfMeasure.ENERGY.GWH]: {
        [UnitOfMeasure.ENERGY.KWH]: 1000000,
        [UnitOfMeasure.ENERGY.MWH]: 1000,
        [UnitOfMeasure.ENERGY.TWH]: 0.001,
        [UnitOfMeasure.ENERGY.JOULE]: 3.6e12,
    },
    [UnitOfMeasure.ENERGY.TWH]: {
        [UnitOfMeasure.ENERGY.KWH]: 1e9,
        [UnitOfMeasure.ENERGY.MWH]: 1e6,
        [UnitOfMeasure.ENERGY.GWH]: 1e3,
        [UnitOfMeasure.ENERGY.JOULE]: 3.6e15,
    },
    [UnitOfMeasure.ENERGY.JOULE]: {
        [UnitOfMeasure.ENERGY.KWH]: 2.778e-7,
        [UnitOfMeasure.ENERGY.MWH]: 2.778e-10,
        [UnitOfMeasure.ENERGY.GWH]: 2.778e-13,
        [UnitOfMeasure.ENERGY.TWH]: 2.778e-16,
        [UnitOfMeasure.ENERGY.KILO_JOULE]: 0.001,
    },
    [UnitOfMeasure.ENERGY.KILO_JOULE]: {
        [UnitOfMeasure.ENERGY.KWH]: 2.778e-4,
        [UnitOfMeasure.ENERGY.MWH]: 2.778e-7,
        [UnitOfMeasure.ENERGY.GWH]: 2.778e-10,
        [UnitOfMeasure.ENERGY.TWH]: 2.778e-13,
        [UnitOfMeasure.ENERGY.JOULE]: 1000,
    },

    // Time.
    [UnitOfMeasure.TIME.MILLISECOND]: {
        [UnitOfMeasure.TIME.MILLISECOND]: 1,
        [UnitOfMeasure.TIME.MINUTE]: 1 / (1000 * 60),
        [UnitOfMeasure.TIME.HOUR]: 1 / (1000 * 60 * 60),
        [UnitOfMeasure.TIME.DAY]: 1 / (1000 * 60 * 60 * 24),
        [UnitOfMeasure.TIME.WEEK]: 1 / (1000 * 60 * 60 * 24 * 7),
        [UnitOfMeasure.TIME.MONTH]: 1 / (1000 * 60 * 60 * 24 * 30.437),
        [UnitOfMeasure.TIME.YEAR]: 1 / (1000 * 60 * 60 * 24 * 365.25),
        [UnitOfMeasure.TIME.DECADE]: 1,
        [UnitOfMeasure.TIME.CENTURY]: 1,
    },
    [UnitOfMeasure.TIME.SECOND]: {
        [UnitOfMeasure.TIME.MILLISECOND]: 1000,
        [UnitOfMeasure.TIME.MINUTE]: 1,
        [UnitOfMeasure.TIME.HOUR]: 1,
        [UnitOfMeasure.TIME.DAY]: 1,
        [UnitOfMeasure.TIME.WEEK]: 1,
        [UnitOfMeasure.TIME.MONTH]: 1,
        [UnitOfMeasure.TIME.YEAR]: 1,
        [UnitOfMeasure.TIME.DECADE]: 1,
        [UnitOfMeasure.TIME.CENTURY]: 1,
    },
    [UnitOfMeasure.TIME.MINUTE]: {
        [UnitOfMeasure.TIME.MILLISECOND]: 1000 * 60,
        [UnitOfMeasure.TIME.SECOND]: 1,
        [UnitOfMeasure.TIME.HOUR]: 1,
        [UnitOfMeasure.TIME.DAY]: 1,
        [UnitOfMeasure.TIME.WEEK]: 1,
        [UnitOfMeasure.TIME.MONTH]: 1,
        [UnitOfMeasure.TIME.YEAR]: 1,
        [UnitOfMeasure.TIME.DECADE]: 1,
        [UnitOfMeasure.TIME.CENTURY]: 1,
    },
    [UnitOfMeasure.TIME.HOUR]: {
        [UnitOfMeasure.TIME.MILLISECOND]: 1000 * 60 * 60,
        [UnitOfMeasure.TIME.SECOND]: 1,
        [UnitOfMeasure.TIME.MINUTE]: 1,
        [UnitOfMeasure.TIME.DAY]: 1,
        [UnitOfMeasure.TIME.WEEK]: 1,
        [UnitOfMeasure.TIME.MONTH]: 1,
        [UnitOfMeasure.TIME.YEAR]: 1,
        [UnitOfMeasure.TIME.DECADE]: 1,
        [UnitOfMeasure.TIME.CENTURY]: 1,
    },
    [UnitOfMeasure.TIME.DAY]: {
        [UnitOfMeasure.TIME.MILLISECOND]: 1000 * 60 * 60 * 24,
        [UnitOfMeasure.TIME.SECOND]: 1,
        [UnitOfMeasure.TIME.MINUTE]: 1,
        [UnitOfMeasure.TIME.HOUR]: 1,
        [UnitOfMeasure.TIME.WEEK]: 1,
        [UnitOfMeasure.TIME.MONTH]: 1,
        [UnitOfMeasure.TIME.YEAR]: 1,
        [UnitOfMeasure.TIME.DECADE]: 1,
        [UnitOfMeasure.TIME.CENTURY]: 1,
    },
    [UnitOfMeasure.TIME.WEEK]: {
        [UnitOfMeasure.TIME.MILLISECOND]: 1000 * 60 * 60 * 24 * 7,
        [UnitOfMeasure.TIME.SECOND]: 1,
        [UnitOfMeasure.TIME.MINUTE]: 1,
        [UnitOfMeasure.TIME.HOUR]: 1,
        [UnitOfMeasure.TIME.DAY]: 1,
        [UnitOfMeasure.TIME.MONTH]: 1,
        [UnitOfMeasure.TIME.YEAR]: 1,
        [UnitOfMeasure.TIME.DECADE]: 1,
        [UnitOfMeasure.TIME.CENTURY]: 1,
    },
    [UnitOfMeasure.TIME.MONTH]: {
        [UnitOfMeasure.TIME.MILLISECOND]: 1000 * 60 * 60 * 24 * 30.437,
        [UnitOfMeasure.TIME.SECOND]: 1,
        [UnitOfMeasure.TIME.MINUTE]: 1,
        [UnitOfMeasure.TIME.HOUR]: 1,
        [UnitOfMeasure.TIME.DAY]: 1,
        [UnitOfMeasure.TIME.WEEK]: 1,
        [UnitOfMeasure.TIME.YEAR]: 1,
        [UnitOfMeasure.TIME.DECADE]: 1,
        [UnitOfMeasure.TIME.CENTURY]: 1,
    },
    [UnitOfMeasure.TIME.YEAR]: {
        [UnitOfMeasure.TIME.MILLISECOND]: 1000 * 60 * 60 * 24 * 365.25,
        [UnitOfMeasure.TIME.SECOND]: 3.154e7,
        [UnitOfMeasure.TIME.MINUTE]: 525600,
        [UnitOfMeasure.TIME.HOUR]: 8760,
        [UnitOfMeasure.TIME.DAY]: 365,
        [UnitOfMeasure.TIME.WEEK]: 52.14,
        [UnitOfMeasure.TIME.MONTH]: 12,
        [UnitOfMeasure.TIME.DECADE]: 0.1,
        [UnitOfMeasure.TIME.CENTURY]: 0.01,
    },
    [UnitOfMeasure.TIME.DECADE]: {
        [UnitOfMeasure.TIME.SECOND]: 1,
        [UnitOfMeasure.TIME.MINUTE]: 1,
        [UnitOfMeasure.TIME.HOUR]: 1,
        [UnitOfMeasure.TIME.DAY]: 1,
        [UnitOfMeasure.TIME.WEEK]: 1,
        [UnitOfMeasure.TIME.MONTH]: 1,
        [UnitOfMeasure.TIME.YEAR]: 1,
        [UnitOfMeasure.TIME.CENTURY]: 1,
    },
    [UnitOfMeasure.TIME.CENTURY]: {
        [UnitOfMeasure.TIME.SECOND]: 1,
        [UnitOfMeasure.TIME.MINUTE]: 1,
        [UnitOfMeasure.TIME.HOUR]: 1,
        [UnitOfMeasure.TIME.DAY]: 1,
        [UnitOfMeasure.TIME.WEEK]: 1,
        [UnitOfMeasure.TIME.MONTH]: 1,
        [UnitOfMeasure.TIME.YEAR]: 1,
        [UnitOfMeasure.TIME.DECADE]: 1,
    },
};