/**
 * @class
 */
class Page {

    /**
     * @private
     */
    static _addEvent(name, f) {
        let old = window[name];

        if (typeof window[name] !== 'function') {
            window[name] = f;
        } else {
            window.onload = () => {
                if (old) old();

                f();
            }
        }
    }
    
    /**
     * https://www.htmlgoodies.com/beyond/javascript/article.php/3724571/Using-Multiple-JavaScript-Onload-Functions.htm
     */
    static addLoadEvent(f) {
        this._addEvent('onload', f);
    }

    /**
     * @public
     */
    static addResizeEvent(f) {
        this._addEvent('onresize', f);
    }

    /**
     * @public
     */
    static getSize() {
        const w = window,
        d = document,
        e = d.documentElement,
        g = d.getElementsByTagName('body')[0],
        x = w.innerWidth || e.clientWidth || g.clientWidth,
        y = w.innerHeight|| e.clientHeight|| g.clientHeight;

        return {width: x, height: y};
    }
}
