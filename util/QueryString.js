/**
 * @class
 * Provides functions for interacting with the URL query string.
 */
class QueryString {
   
    /**
     * @return {Object<string, Object>}
     */
    static getAsObj() {
        const qstr = window.location.search.substring(1);
        
        return this.parse(qstr);
    }

    /**
     * @public
     * https://stackoverflow.com/questions/2090551/parse-query-string-in-javascript
     */
    static parse(qstr) {
        const a = (qstr[0] === '?' ? qstr.substr(1) : qstr).split('&');
        let query = {};

        for (let i = 0; i < a.length; i++) {
            const b = a[i].split('=');
            const paramName = decodeURIComponent(b[0]);
            const value = decodeURIComponent(b[1] || '');

            if (!query.hasOwnProperty(paramName)) {
                query[paramName] = value;
            } else {
                if (!Array.isArray(query[paramName])) {
                    query[paramName] = [query[paramName]];
                }
                query[paramName].push(value);
            }
        }

        return query;
    }

    /**
     * Builds a query URL.
     * @param {String} endpoint
     * @param {Object<String, Object>} data
     * @return {String}
     */
    static build(endpoint, data, encode) {
        if (encode === undefined) encode = true;
        let result = endpoint + '?';

        for (let key in data) {
            let datum = data[key];

            if (Array.isArray(datum)) {
                datum.forEach(item => {
                    if (encode) item = encodeURIComponent(item);
                    result = result + key + '=' + item + '&';  
                });
            } else {
                if (encode) datum = encodeURIComponent(datum);
                result = result + key + '=' + datum + '&';
            }
        }

        return result.slice(0, result.length-1);
    }
}