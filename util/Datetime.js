// nodist

/**
 * A date/time library.
 * @property {String} VALID_TIME_STRING_REGEX
 * @property {Number} ONE_DAY_MS
 */
class Datetime {

    /** @private */
    static _pad(s) {
        s = String(s);
        while (s.length < 2) {
            s = '0' + s;
        }
        return s;
    }

    /**
     * @private
     */
    static _cleanDate(date) {
        if (typeof date === 'string' && date.indexOf('T') === -1) {
            date = date.split('-').join('/');
        }

        return new Date(date);
    }

    /**
     * Converts a Date to a human-readable string of the form YYYY-MM-DD.
     * @static
     * @param {Date|Number} date
     * @return {String}
     */
    static dateToDateString(date) {
        if (!date) return 'Not specified';
        date = this._cleanDate(date);

        return date.getFullYear() + '-'
            + Datetime._pad(date.getMonth() + 1)
            + '-' + Datetime._pad(date.getDate());
    }

    /**
     * Converts a Date to a human-readable string of the form DD Month YYYY.
     * @static
     * @param {Date|Number} date
     * @return {String}
     */
    static dateToDateString2(date) {
        if (!date) return 'Not specified';

        date = this._cleanDate(date);

        const year = date.getFullYear();
        const month = Datetime.MONTHS[date.getMonth()];
        const day = date.getDate();

        return `${day} ${month} ${year}`;
    }

    /**
     * Converts a Date representing a duration of time to a human-readable string.
     * @static
     * @param {Date|Number} date
     * @returns {string}
     */
    static dateToTimeString(date) {
        if (!date) return 'Not specified';
        date = this._cleanDate(date);

        return Datetime._pad(date.getHours()) + ':' + Datetime._pad(date.getMinutes());
    }

    /**
     * @public
     */
    static dateToDateTimeString(date) {
        if (!date) return 'Not specified';
        date = this._cleanDate(date);

        return Datetime.dateToDateString(date) + ' ' + Datetime.dateToTimeString(date);
    }
    
    /**
     * @public
     * @param {Date} date - date in UTC
     * @return {Boolean}
     */
    static isToday(date) {
        const today = new Date();

        return date.getDate() === today.getDate() && 
            date.getMonth() === today.getMonth() && 
            date.getFullYear() === today.getFullYear();
    } 

    // TODO: Move to Duration.
    /**
     * Converts milliseconds to a human-readable string.
     * @static
     * @param {Number} ms - Milliseconds since the epoch.
     * @returns {string}
     */
    static msToString(ms, useSeconds) {
        if (useSeconds === undefined) useSeconds = true;

        if (ms < 0) {
            return 'EXPIRED!';
        }

        const days = Math.floor(ms / Datetime.ONE_DAY_MS);
        const hours = Math.floor((ms % Datetime.ONE_DAY_MS) / Datetime.ONE_HOUR_MS);
        const minutes = Math.floor((ms % Datetime.ONE_HOUR_MS) / ONE_MINUTE_MS);
        const seconds = Math.floor((ms % ONE_MINUTE_MS) / 1000);
        let result = '';

        if (useSeconds) result = seconds + ' seconds';

        if (minutes > 0 || hours > 0 || days > 0) {
            result = minutes + ' minutes ' + result;
        }
        if (hours > 0 || days > 0) {
            result = hours + ' hours ' + result;
        }
        if (days > 0) {
            result = days + ' days ' + result;
        }

        return result;
    }

    /**
     * @return {Number} - The current time in milliseconds.
     */
    static getTime() {
        return (new Date()).getTime();
    }

    /**
     * Returns an integer representing the current week.
     * @param {Number} ms - The current time in milliseconds since the epoch.
     * @returns {Number} - The current week.
     */
    static getWeek(ms) {
        return Math.floor(ms / ONE_WEEK_MS);
    }

    /**
     * @public
     */
    static dateToMysqlDate(date) {
        if (!date) return 'Not specified';
        date = this._cleanDate(date);

        return date.getUTCFullYear() + '-' +
            ('00' + (date.getUTCMonth()+1)).slice(-2) + '-' +
            ('00' + date.getUTCDate()).slice(-2) + ' ' +
            ('00' + date.getUTCHours()).slice(-2) + ':' +
            ('00' + date.getUTCMinutes()).slice(-2) + ':' +
            ('00' + date.getUTCSeconds()).slice(-2);
    }

    /**
     * @public
     */
    static militaryTimeTo12Hour(timeString) {
        const parts = timeString.split(':');
        let hours = Number(parts[0]);
        let minutes = Number(parts[1]);
        let suffix = 'AM';

        if (hours === 12 && minutes === 0) {
            suffix = 'noon';
        } else if (hours >= 12) {
            suffix = 'PM';
        }

        if (hours === 0 && minutes === 0) {
            return '12:00 midnight';
        } else if (hours === 0) {
            hours = 12;
        }

        if (hours > 12) {
            hours = hours % 12;
        }

        minutes = this._pad(minutes);

        return `${hours}:${minutes} ${suffix}`;
    }

    /**
     * @public
     */
    static getFirstOfThisMonth() {
        const today = new Date();

        return new Date(today.getFullYear(), today.getMonth(), 1);
    }

    /**
     * @public
     */
    static setToStartOfDay(date) {
        date = this._cleanDate(date);

        date.setHours(0, 0, 0, 0);

        return date;
    }

    /**
     * @public
     */
    static setToEndOfDay(date) {
        date = this._cleanDate(date);

        date.setHours(23, 59, 59, 999);

        return date;
    }

    /**
     * @public
     */
    static addTimezoneOffset(date) {
        date = this._cleanDate(date);

        date.setTime(date.getTime() + date.getTimezoneOffset()*60*1000)

        return date;
    }

    /**
     * @public
     */
    static subtractTimezoneOffset(date) {
        date = this._cleanDate(date);

        date.setTime(date.getTime() - date.getTimezoneOffset()*60*1000)

        return date;
    }

    /**
     * @public
     */
    static addDays(date, days) {
        date = this._cleanDate(date);

        date.setDate(date.getDate() + days);

        return date;
    }

    /**
     * @public
     */
    static getDifferenceDays(date0, date1) {
        date0 = this._cleanDate(date0);
        date1 = this._cleanDate(date1);

        const diffMs = date0.getTime() - date1.getTime();

        return diffMs / (this.ONE_DAY_MS);
    }
}

Datetime.VALID_TIME_STRING_REGEX = /^[0-9]{2}:[0-9]{2}(:[0-9]{2})?$/;
Datetime.MILLIS_PER_SECOND = 1000;
Datetime.SECONDS_PER_HOUR = 60 * 60;
Datetime.ONE_HOUR_MS = 1000 * 60 * 60;
Datetime.ONE_DAY_MS = Datetime.ONE_HOUR_MS * 24;

// TODO: Make these properties of Datetime
const ONE_WEEK_MS = Datetime.ONE_DAY_MS * 7;
const ONE_MINUTE_MS = 60 * 1000;
const ONE_HOUR_MS = ONE_MINUTE_MS * 60;

Datetime.MONTH = {
    JANUARY: 0,
    FEBRUARY: 1,
    MARCH: 2,
    APRIL: 3,
    MAY: 4,
    JUNE: 5,
    JULY: 6,
    AUGUST: 7,
    SEPTEMBER: 8,
    OCTOBER: 9,
    NOVEMBER: 10,
    DECEMBER: 11,
};

Datetime.MONTHS = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
];

Datetime.DAYS_OF_WEEK = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
];

/**
 * Represents a duration of time.
 * @class
 */
class Duration {

    /**
     * @constructor
     * @param {Number} millis
     */
    constructor(millis) {
        this._millis = millis;
    }

    /**
     * Returns a human-readable string describing this duration.
     */
    toString(seconds) {
        if (seconds === undefined) seconds = true;

        return Datetime.msToString(this._millis, seconds);
    }

    /**
     * Returns a human-readable string describing this duration.
     */
    toShortString() {
        const hours = Math.floor((this._millis % Datetime.ONE_DAY_MS) / Datetime.ONE_HOUR_MS);
        const minutes = Math.floor((this._millis % Datetime.ONE_HOUR_MS) / ONE_MINUTE_MS);
        const seconds = Math.floor((this._millis % ONE_MINUTE_MS) / 1000);
        let result = '';

        if (hours) {
            result = result + Datetime._pad(hours) + ':';
        }

        result = result + Datetime._pad(minutes) + ':' + Datetime._pad(seconds);

        return result;
    }

    /**
     * @public
     * Returns the total duration in seconds.
     */
    seconds() {
        return this._millis / Datetime.MILLIS_PER_SECOND;
    }

    /**
     * @public
     * Returns the number of whole minutes left over after hours are subtracted.
     */
    minutes() {
        const ms = this._millis % ONE_HOUR_MS;

        return Math.floor(ms / ONE_MINUTE_MS)
    }

    /**
     * @public
     * Returns the number of whole hours in the duration.
     */
    hours() {
        return Math.floor(this._millis / ONE_HOUR_MS);
    }
}
