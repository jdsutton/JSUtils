/**
 * @class
 */
class Mixin {

    /**
     * @private
     */
    static _assignMethods(to, from) {
        const blacklist = ['constructor', 'prototype'];
        
        for (let key of Object.getOwnPropertyNames(from)) {
            if (blacklist.indexOf(key) < 0 && typeof from[key] === 'function') {
                to[key] = from[key];
            }
        }
    }

    /**
     * @public
     */
    static combine() {
        let result = class extends arguments[0] {};

        for (let i = 1; i < arguments.length; i++) {
            let C = arguments[i];

            this._assignMethods(result, C);
            this._assignMethods(result.prototype, C.prototype);

            Object.assign(result, C);
        }

        return result;
    }
}