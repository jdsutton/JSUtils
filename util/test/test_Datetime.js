import('../assert.js');
import('../Datetime.js');


suite('Datetime', function() {

    const temp = Date.prototype.getTime;

    beforeEach(function() {
        Date.prototype.getTime = () => {
        return 10000;
        };
    });

    afterEach(function() {
        Date.prototype.getTime = temp;
    });

    // FIXME: Timezone-dependent, as expected, but check format instead of exact.
    // suite('#dateToDateString()', function() {
    //     spec('Should return the date as a human readable string', function() {
    //         const date = new Date('Tue Sep 04 2018 20:31:51 GMT-0700 (Pacific Daylight Time)');
    //         const expected = '2018-09-04';
    //         assertEqual(Datetime.dateToDateString(date), expected);
    //     });
    // });

    // suite('#dateToTimeString()', function() {
    //     spec('Should return military time as human readable string', function() {
    //         const date = new Date('Tue Sep 04 2018 20:31:51 GMT-0700 (Pacific Daylight Time)');
    //         const expected = '20:31';
    //         assertEqual(Datetime.dateToTimeString(date), expected);
    //     });
    // });

    // suite('#dateToDateTimeString', function() {
    //     spec('Should return the time and date as a human readable string', function() {
    //         const date = new Date('Tue Sep 04 2018 20:31:51 GMT-0700 (Pacific Daylight Time)');
    //         const expected = '2018-09-04 20:31';
    //         assertEqual(Datetime.dateToDateTimeString(date), expected);
    //     });
    // });

    // suite('#dateToMysqlDate', function() {
    //     spec('Should return time and date as a Mysql compatable string', function() {
    //         const date = new Date('Tue Sep 04 2018 20:31:51 GMT-0700 (Pacific Daylight Time)');
    //         const expected = '2018-09-05 03:31:51';
    //         assertEqual(Datetime.dateToMysqlDate(date), expected);
    //     });
    // });

    suite('#msToString', function() {
        spec('Should return ms as human readable string', function() {
            const expected = '2 hours 24 minutes 1 seconds';
            assertEqual(Datetime.msToString(8641018), expected);
        });
    });

    suite('#getTime()', function() {
        spec('Should return the current time in ms as a number', function() {
            const expected = 10000; 
            assertEqual(Datetime.getTime(), expected);
        });
    });

    suite('#getWeek()', function() {
        spec('Should return the current week as a number', function() {
            const expected = 0;
            const ms = new Date().getTime();
            assertEqual(Datetime.getWeek(ms), expected);
        });
    });

    suite('#militaryTimeTo12Hour()', function() {
        spec('Should return 12 hour time as a human readable string', function() {
            const expected = '8:31 PM';
            assertEqual(Datetime.militaryTimeTo12Hour('20:31:00'), expected);
        });
    });
});

suite('Duration', function() {
    suite('#toString()', function() {
        spec('Should return duration of time as a human readable string', function() {
            const expected = '10 seconds';
            assertEqual(new Duration(10000).toString(), expected);
        });
    });

    suite('#seconds()', function() {
        spec('Should return duration of time in seconds as a number', function() {
            const expected = 10; 
            assertEqual(new Duration(10000).seconds(), expected);
        });
    });
});