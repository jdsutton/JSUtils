
/**
 * @class
 */
class Timeout {

    /**
     * @public
     */
    static wait(ms) {
        return new Promise((resolve, reject) => {
            setTimeout(resolve, ms);
        });
    }
}