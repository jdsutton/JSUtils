
/**
 * Provides functions for validating forms.
 * @class FormValidation
 */
class FormValidation {
    
    /**
     * @public
     * @param {Object} data
     * @param {Object<string, FormValidation.Validator[]>}
     * @return {Boolean} Whether validation errors were found.
     */
    static validate(formId, data, validatorsByFieldName, removeErrors) {
        if (removeErrors === undefined) removeErrors = true;
        
        let errorsFound = false;

        if (removeErrors) this.removeErrorMessages(formId);

        Object.keys(validatorsByFieldName).forEach(key => {
            if (!key in data) {
                throw new Error('Key not found in form: ' + key);
            }
        });

        Object.keys(data).forEach(key => {

            if (key in validatorsByFieldName) {
                const validators = validatorsByFieldName[key];
                let errors = [];

                validators.forEach((validator) => {
                    const message = validator.validate(data[key]);
                    
                    if (message) errors.push(message);
                });

                if (errors.length) {
                    this.showError(formId, key, errors);

                    errorsFound = true;
                }
            }
        });

        return errorsFound;
    }

    /**
     * Removes form validation errors from the UI.
     * @public
     * @param {String} formId - The ID of the form element.
     */
    static removeErrorMessages(formId) {
        const form = Dom.getById(formId);
        const inputs = Array.from(form.getElementsByTagName('INPUT'));

        inputs.forEach((input) => {
            Dom.removeClass(input,
                FormValidation.CLASS.VALIDATION_FAILED);
        });

        const errors = Array.from(Dom.getByClass(
            FormValidation.CLASS.VALIDATION_ERROR));
        
        errors.forEach((error) => {
            Dom.removeElement(error);
        });
    }

    /**
     * Shows form validation error messages in the UI.
     * @public
     * @param {String} formId - The ID of the form element.
     * @param {String} inputName - The name of the input in the form which has an error.
     * @param {String} message - The message to show.
     */
    static showError(formId, inputName, message) {
        const form = Dom.getById(formId);
        const input = Dom.getChildrenByName(form, inputName)[0];
        const element = document.createElement('div');

        Dom.addClass(input, FormValidation.CLASS.VALIDATION_FAILED);
        element.className = FormValidation.CLASS.VALIDATION_ERROR;
        element.innerHTML = message;
        Dom.insertAfter(input, element);
    }

    /**
     * Displays error messages on the given form.
     * @public
     * @param {String} formId - The ID of the form element.
     * @param {Object<string, string[]>}
     */
    static showErrorMessages(formId, errors) {
        const fieldNames = Object.keys(errors);

        fieldNames.forEach(fieldName => {
            if (errors[fieldName].length) {
                this.showError(formId, fieldName, errors[fieldName]);
            }
        });
    }
}

FormValidation.CLASS = {
    VALIDATION_ERROR: 'ValidationError',
    VALIDATION_FAILED: 'ValidationFailed'
};

/**
 * @interface FormValidation.Validator
 */
FormValidation.Validator = class {

    /**
     * @constructor
     */
    constructor() {
        this._nullable = false;
    }

    /**
     * Returns an error message if the value is invalid.
     * @public
     * @param {Object} value
     * @return {?String}
     */
    validate(value) {

    }

    /**
     * @public
     */
    isNullAndValid(value) {
        return this._nullable && !value.length;
    }

    /**
     * @public
     * Tells the Validator to allow empty values.
     */
    setNullable() {
        this._nullable = true;

        return this;
    }
}

/**
 * @class FormValidation.MaxLengthValidator
 * @extends FormValidation.Validator
 * @param {Number} maxLength
 */
FormValidation.MaxLengthValidator = class extends FormValidation.Validator {
    constructor(maxLength) {
        super();
        this._maxLength = maxLength;
    }

    /**
     * Returns an error message if the value is too long.
     * @public
     * @param {Object} value
     * @return {?String}
     */
    validate(value) {
        if (this.isNullAndValid(value)) return null;
        
        if (value.length > this._maxLength) {
            return 'Max field length is: ' + this._maxLength + ' characters.';
        }

        return null;
    }
}

/**
 * @class FormValidation.MinLengthValidator
 * @extends FormValidation.Validator
 * @param {Number} minLength
 */
FormValidation.MinLengthValidator = class extends FormValidation.Validator {
    
    /**
     * @public
     * @param {Number} minLength
     */
    constructor(minLength) {
        super();

        this._minLength = minLength;
    }

    /**
     * Returns an error message if the value is too short.
     * @public
     * @param {Object} value
     * @return {?String}
     */
    validate(value) {
        if (this.isNullAndValid(value)) return null;
        
        if (value.length < this._minLength) {
            return 'Min field length is: ' + this._minLength + ' characters.';
        }

        return null;
    }
}

/**
 * @class FormValidation.RangeValidator
 * @extends FormValidation.Validator
 * @param {Number} min
 * @param {Number} max
 */
FormValidation.RangeValidator = class extends FormValidation.Validator {
   
    /**
     * @public
     * @param {Number} min
     * @param {Number} max
     */
   constructor(min, max) {
        super();
        this._min = min;
        this._max = max;
    }

    /**
     * Returns an error message if the value is out of range.
     * @public
     * @param {Object} value
     * @return {?String}
     */
    validate(value) {
        if (this.isNullAndValid(value)) return null;
        
        if (!value) {
            return 'Value must not be empty.';
        }

        if (!FormValidation.RangeValidator._numberRegex.test(value)) {
            return 'Value must be a number';
        }
        if (typeof this._min === 'number' && Number(value) < this._min) {
            return 'Value must be at least: ' + this._min;
        }
        if (typeof this._max === 'number' && Number(value) > this._max) {
            return 'Value must be at most: ' + this._max;
        }

        return null;
    }
}
FormValidation.RangeValidator._numberRegex = /^[-+]?[0-9]+(\.[0-9]+)?$/;

/**
 * @class FormValidation.USDValidator
 * @extends FormValidation.Validator
 */
FormValidation.USDValidator = class extends FormValidation.Validator {
    
    /**
     * @public
     * @param {String} value
     */
    validate(value) {
        if (this.isNullAndValid(value)) return null;
        
        const valueIsNull = (value === '' || value === 'null');

        if (this._nullable && valueIsNull) return null;

        if (!FormValidation.USDValidator._DOLLAR_AMOUNT_REGEX.test(value)) {
            return 'Value must be a valid USD amount.';
        }
   }
}
FormValidation.USDValidator._DOLLAR_AMOUNT_REGEX = /^\$?(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$/;

/**
 * @class FormValidation.HTMLInjectionValidator
 * @extends FormValidation.Validator
 */
FormValidation.HTMLInjectionValidator = class extends FormValidation.Validator {
    
    /**
     * Returns an error message if the value contains an HTML tag.
     * @public
     * @param {Object} value
     * @return {?String}
     */
    validate(value) {
        if (this.isNullAndValid(value)) return null;
        
        if (FormValidation.HTMLInjectionValidator._htmlTagRegex.test(value)) {
            return 'No HTML tags allowed.';
        }        

        return null;
    }
}
FormValidation.HTMLInjectionValidator._htmlTagRegex = /<[a-zA-Z]+[\s=a-zA-Z0-9\"\/:\.]*>/g;

/**
 * @class FormValidation.PhoneNumberValidator
 * @extends FormValidation.Validator
 */
FormValidation.PhoneNumberValidator = class extends FormValidation.Validator {
    
    /**
     * Returns an error message if the value is not a valid US phone number.
     * @public
     * @param {Object} value
     * @return {?String}
     */
    validate(value) {
        if (this.isNullAndValid(value)) return null;
        
        // Remove non-numeric characters.
        value = value.replace(/\D/g,'');

        if (value.length < 10 || value.length > 11) {
            return 'Please enter a valid US phone number.';
        }        

        return null;
    }
}
// FormValidation.PhoneNumberValidator._regex = /^([0-9]( |-)?)?(\(?[0-9]{3}\)?|[0-9]{3})( |-)?([0-9]{3}( |-)?[0-9]{4}|[a-zA-Z0-9]{7})$/;

/**
 * @class FormValidation.EmailValidator
 * @extends FormValidation.Validator
 */
FormValidation.EmailValidator = class extends FormValidation.Validator {
    
    /**
     * Returns an error message if the value is not a valid email address.
     * @public
     * @param {Object} value
     * @return {?String}
     */
    validate(value) {
        if (this.isNullAndValid(value)) return null;
        
        value = value.trim();
        value = value.toLowerCase();
        
        if (!FormValidation.EmailValidator._regex.test(value)) {
            return 'Please enter a valid email address.';
        }        

        return null;
    }
}
FormValidation.EmailValidator._regex = /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;

/**
 * @class FormValidation.FieldsMatchValidator
 * @extends FormValidation.Validator
 */
FormValidation.FieldsMatchValidator = class extends FormValidation.Validator {
    
    /**
     * @public
     * @param {String} formId
     * @param {String} fieldName
     */
    constructor(formId, fieldName) {
        super();
        this._formId = formId;
        this._fieldName = fieldName;
    }

    /**
     * Returns an error message if the value does not match the value of another field.
     * @param {Object} value
     * @return {?String}
     */
    validate(value) {
        if (this.isNullAndValid(value)) return null;
        
        const match = Dom.getDataFromForm(this._formId)[this._fieldName];   

        if (value !== match) {
            return 'Fields must match';
        }

        return null;
    }
}

/**
 * @class FormValidation.WholeNumberValidator
 * @extends FormValidation.Validator
 */
FormValidation.WholeNumberValidator = class extends FormValidation.Validator {
    
    /**
     * Returns an error message if the value is not a whole number.
     * @public
     * @param {Object} value
     * @return {?String}
     */
    validate(value) {
        if (this.isNullAndValid(value)) return null;
        
        if (!this.constructor._REGEX.test(value)) {
            return 'Value must be a whole number';
        }

        return null;
    }
}
FormValidation.WholeNumberValidator._REGEX = /^(0|-?[1-9][0-9]*)$/;

/**
 * @class FormValidation.DatesOrderedValidator
 * @extends FormValidation.Validator
 */
FormValidation.DatesOrderedValidator = class extends FormValidation.Validator {
    
    /**
     * @public
     * @param {String} formId
     * @param {Array<String>}
     */
    constructor(formId, fields) {
        super();
        this._formId = formId;
        this._fields = fields;
    }

    /**
     * @public
     */
    validate() {
        if (this.isNullAndValid(value)) return null;
        
        const data = Dom.getDataFromForm(this._formId);
        const values = this._fields.map(field => {
            return new Date(data[field]);
        });

        for (let i = 1; i < values.length; i++) {
            if (values[i - 1] > values[i]) return 'Dates must be in order.';
        }

        return null;
    }
}

/**
 * @class FormValidation.FunctionValidator
 * @extends FormValidation.Validator
 */
FormValidation.FunctionValidator = class extends FormValidation.Validator {
    
    /**
     * @public
     * @param {Function} f
     * @param {String} message
     */
    constructor(f, message) {
        super();

        this._f = f;
        this._message = message || 'Invalid value';
    }

    /**
     * Returns an error message if the value is not a whole number.
     * @public
     * @param {Object} value
     * @return {?String}
     */
    validate(value) {
        if (this.isNullAndValid(value)) return null;
        
        if (!this._f(value)) {
            return this._message;
        }

        return null;
    }
}