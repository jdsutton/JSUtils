import('Dom.js');
import('Request.js');

/**
 * @class
 * @public
 * Loads code and other resources onto the page asynchronously.
 */
class LoadAsync {
    
    /**
     * @public
     * @param {String} uri
     */
    static loadJavaScript(uri) {
        return Request.get(uri, /** crossOrigin*/ true).then(data => {
            let scriptElement = document.createElement('script');

            scriptElement.type = 'text/javascript';
            scriptElement.innerHTML = data;

            let firstScriptElement = document.getElementsByTagName('script')[0];
            firstScriptElement.parentNode.insertBefore(scriptElement,
                firstScriptElement);
        });
    }

    /**
     * @public
     * @param {String} uri
     * @param {Element} container
     */
    static loadHTML(uri, container) {
        if (this._urisLoaded.has(uri)) return Promise.resolve();

        return Request.get(uri, /** crossOrigin */ true).then(data => {
            Dom.appendTo(container, data);
            
            return Promise.resolve();
        });
    }

    /**
     * @public
     */
    static loadImage(uri, container) {
        let img = new Image();

        console.debug(`Loading image: ${uri}`);

        return new Promise((resolve, reject) => {
            img.onload = () => {

                if (container) {
                    const html = Dom.elementToHTML(img);

                    Dom.appendTo(container, html);
                }

                resolve(img);
            };

            img.src = uri;
        });
    }
}

LoadAsync._urisLoaded = new Set();