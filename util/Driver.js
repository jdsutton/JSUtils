import('../components/InfiniteScrollList.js');

/**
 * @class
 * View driver.
 */
class Driver {
    
    /**
     * @public
     * Sets up the view.
     * Typically called when a page first loads.
     */
    static init() {
        Driver._driver = Driver._driver || this;
    }

    /**
     * @public
     * Returns the first driver initialized on the given page.
     */
    static getViewDriver() {
        return Driver._driver;
    }

    /**
     * @private
     */
    static _listAndRender(containerId, loadF, component) {
        const renderF = app => {
            return new component(app).render();
        };

        return new InfiniteScrollList(
            containerId,
            null,
            renderF,
            loadF,
        ).loadMore();
    }

    /**
     * Returns a component of the given class if found in the event path.
     * @param {String} event - A DOM event.
     * @param {Function} componentClass - A class descended from Component.
     * @return {?Component}
     * @private
     */
    static _tryGetComponent(event, componentClass) {
        const className = componentClass.name;
        const path = event.path || (event.composedPath && event.composedPath());

        for (let element of path) {
            if (Dom.hasClass(element, className)) {
                return Component.get(element.id);
            }  
        }

        return null;
    }

    /**
     * Returns an elementif found in the event path.
     * @param {String} event - A DOM event.
     * @param {Function} matchF - A function returning true if the element is to be returned.
     * @retun {?Element}
     * @private
     */
    static _tryGetElement(event, matchF) {
        const path = event.path || (event.composedPath && event.composedPath());

        for (let element of path) {
            if (matchF(element)) {
                return element;
            }  
        }

        return null;
    }
}

Driver._driver = null;
